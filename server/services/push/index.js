'use strict';

var rpc = require('../../common/amqp/amqp-rpc');
var pushService = require('./push.js');

function PushService() {
    rpc.on('post/push/apple', pushService.sendApple);
    rpc.on('get/push/apple/test', pushService.sendAppleTest);
}

module.exports = PushService;
