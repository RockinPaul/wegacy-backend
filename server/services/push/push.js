'use strict';

var pushService = module.exports;
var apnagent = require('apnagent');
var agent = new apnagent.Agent();
var async = require('async');
var db = require('../../models');
var logger = require('../../config/logger');
var gcm = require('node-gcm');

var join = require('path').join;
var pfx = join(__dirname, '../../cert/production2.p12');


agent.on('message:error', function(err, msg) {
    console.error(err, msg);
    switch (err.name) {
        // This error occurs when Apple reports an issue parsing the message.
        case 'GatewayNotificationError':
            console.log('[message:error] GatewayNotificationError: %s', err.message);

        // The err.code is the number that Apple reports.
        // Example: 8 means the token supplied is invalid or not subscribed
        // to notifications for your application.
        if (err.code === 8) {
            console.log('    > %s', msg.device().toString());
            // In production you should flag this token as invalid and not
            // send any futher messages to it until you confirm validity
        }

        break;

        // This happens when apnagent has a problem encoding the message for transfer
        case 'SerializationError':
            console.log('[message:error] SerializationError: %s', err.message);
        break;

        // unlikely, but could occur if trying to send over a dead socket
        default:
            console.log('[message:error] other error: %s', err.message);
        break;
    }
});
agent.set('pfx file', pfx);
var connected = false;

console.info('start connect push service', pfx);
agent.connect(function(err) {
    // gracefully handle auth problems
    console.info('connect err', err);
    if (err && err.name === 'GatewayAuthorizationError') {
        console.log('Authentication Error: %s', err.message);
    }
    connected = true;

    // it worked!
    var env = agent.enabled('sandbox') ? 'sandbox' : 'production';

    console.log('apnagent [%s] gateway connected', env);
});

var sendPush = function(token, message, cb) {
    if (!token) {
        return cb('need device token');
    }
    //agent.enable('sandbox');
    console.info('send to', token, 'message: ', message);
    agent.createMessage().device(token).alert(message).send();
    return cb(null);
};

var sendPushAndroid = function(token, message, cb) {
    if (!token) {
        return cb(null, 'need device token');
    }
    var message = new gcm.Message({
        title: 'goBlind notification',
        body: message,
        data: {
            message: message
        },
        icon: 'ic_launcher_goblind'
    });

    var regIds = [token];

    // Set up the sender with you API key
    var sender = new gcm.Sender('AIzaSyBVnlFtYkgCcga7fRgpc8S9DTlQr4Ee-Mg');

    // Now the sender can be used to send messages
    sender.send(message, { registrationIds: regIds }, function (err, result) {
        if(err) {
            return cb(null);
        }
        else {
            console.info('send to Android', token, 'message: ', message);
            return cb(null, result);
        }
    });
};

var messageBuilder = function(type, fromUser) {
    var fullName = fromUser ? fromUser.fullName : 'user';
    switch (type) {
        case 'accept':
            return 'Match accepted by "' + fullName + '"';
        case 'refuse':
            return 'Match refused by "' + fullName + '"';
        case 'code':
            return 'Match code accepted by "' + fullName + '". Can start new search.';
        case 'searched':
            return 'Found match with "' + fullName + '"';
    }
};

pushService.sendAppleTest = function(req, callback) {
    if(!connected){
        console.info('apple push service not connected yet');
        return callback(null);
    }
    var userId = req.query.userId || req.user.id;
    console.info('send to userId', userId);
    async.waterfall([
        function(cb){
            db.models.user.find({
                where: {
                    id: userId
                }
            }).then(function(user){
                if(!user.isNotifyServiceNotification){
                    return cb('push off');
                }
                cb(null, user);
            }).catch();
        },
        function(sendToUser, cb) {
            if(!sendToUser){
                return cb(null);
            }
            console.info('send to ', sendToUser.iosAPIKey);
            sendPush(sendToUser.iosAPIKey, 'test push message', function(err) {
                cb(err);
            });
        }
    ], function(err) {
        callback(err, {});
    });
};

pushService.sendApple = function(req, callback) {
    if(!connected){
        console.info('apple push service not connected yet');
        return callback(null);
    }
    async.waterfall([

        function(cb) {
            var sendToUser, fromUser;
            if (req.user.id === req.query.match.user.id) {
                sendToUser = req.query.match.matchedUser;
                fromUser = req.query.match.user;
            } else if (req.user.id === req.query.match.matchedUser.id) {
                sendToUser = req.query.match.user;
                fromUser = req.query.match.matchedUser;
            }
            cb(null, sendToUser, fromUser);
        },
        function(sendToUser, fromUser, cb) {
            console.info('\r\nPUSH to Android', messageBuilder(req.query.type, fromUser));
            if(!sendToUser){
                return cb(null, sendToUser, fromUser);
            }
            sendPushAndroid(sendToUser.googleAPIKey, messageBuilder(req.query.type, fromUser), function(err) {
            });
            return cb(null, sendToUser, fromUser);
        },
        function(sendToUser, fromUser, cb) {
            console.info('\r\nPUSH', messageBuilder(req.query.type, fromUser));
            if(!sendToUser){
                return cb(null);
            }
            sendPush(sendToUser.iosAPIKey, messageBuilder(req.query.type, fromUser), function(err) {
            });
            cb(null);
        }
    ], function(err) {
        callback(err, {});
    });
};
