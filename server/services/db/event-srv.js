'use strict';
var async = require('async');
var db = require('../../models');
var userFriend = require('./user.friend');
var addGoingStatistics = require('../../common/add-going-stats');

var eventSrv = module.exports;

eventSrv.test = function(query, cb) {
    cb(null, {
        answer: 1
    });
};

var getEvents = function(req, searchParams, callback) {
    async.waterfall([
        function(cb) {
            db.models.event.findAndCountAll(searchParams).complete(function(
                err, eventsPaging) {
                cb(err, eventsPaging);
            });
        },
        function(eventsPaging, cb) {
            userFriend.getFriendsIds(req.user.id, function(err, friendsIds) {
                cb(err, req.user.id, eventsPaging, friendsIds);
            });
        },
        function(userId, eventsPaging, friendsIds, cb) {
            async.map(eventsPaging.rows, function(event, cbMap) {
                addGoingStatistics(req, userId, event, friendsIds, cbMap);
            }, function(err, eventsWithGoing) {
                cb(null, {
                    count: eventsPaging.count,
                    rows: eventsWithGoing
                });
            });
        }
    ], function(err, events) {
        callback(err, events);
    });
};
/**
 * @api {get} api/db/events Request All Events
 * @apiName GetEvents
 * @apiGroup Event
 * @apiPermission userSelf
 * @apiDescription Returns array of events of the user {count: 2, rows: []}
 *
 * @apiParam {Guid} userId Optional param if need events of other user
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 *
 * @apiUse EventSuccess
 * @apiError 400 JSON has information about error
 */
eventSrv.getAllEvents = function(req, callback) {
    var searchParams = {
        where: {
            userId: req.query.userId || req.user.id
        },
        include: [{
            model: db.models.user,
            as: 'userGoing',
            attributes: ['id', 'avatar', 'firstName', 'lastName',
                'middleName'
            ],
            where: {
                'id': req.query.userId || req.user.id
            },
            required: false
        }, {
            model: db.models.user,
            as: 'userLike',
            attributes: ['id', 'avatar']
        }],
        limit: req.query.limit || 100,
        offset: req.query.offset || 0,
        order: [['updatedAt', 'DESC']]
    };
    console.info(searchParams);
    getEvents(req, searchParams, callback);
};

/**
 * @api {get} api/db/events/visited Request All Visited Events
 * @apiName GetVisitedEvents
 * @apiGroup Event
 * @apiPermission user
 * @apiDescription Returns array of events {count: 2, rows: []}
 *
 * @apiParam {Guid} userId Optional param if need events of other user
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 *
 * @apiUse EventSuccess
 * @apiError 400 JSON has information about error
 */
eventSrv.getVisitedEvents = function(req, callback) {
    var searchParams = {
        where: {
            endDateTime: {
                lte: new Date()
            }
        },
        include: [{
            model: db.models.user,
            as: 'userGoing',
            attributes: ['id', 'avatar', 'firstName', 'lastName',
                'middleName'
            ],
            where: {
                'id': req.query.userId || req.user.id
            },
            required: true
        }, {
            model: db.models.user,
            as: 'userLike',
            attributes: ['id', 'avatar']
        }],
        limit: req.query.limit || 100,
        offset: req.query.offset || 0,
        order: [['updatedAt', 'DESC']]
    };
    getEvents(req, searchParams, callback);
};

/**
 * @api {get} api/db/event/:id Request Event information by Id
 * @apiName GetEvent
 * @apiGroup Event
 * @apiPermission Admin or UserSelf
 *
 * @apiParam {UUID} id
 *
 * @apiUse EventSuccess
 * @apiSuccess {array} going/avatars array of photo two users going to event
 * @apiSuccess {integer} going/friendsCount
 * @apiSuccess {integer} going/othersCount
 * @apiSuccess {boolean} going/iAmGoing
 * @apiSuccess {boolean} going/iAmLiked The user Like the event
 * @apiSuccess {boolean} canEdit is user can edit the event
 *
 * @apiError 400 JSON has information about error
 */
eventSrv.getEventById = function(req, callback) {
    async.waterfall([
        function(cb) {
            db.models.event.find({
                where: {
                    id: req.params.id
                },
                include: [{
                    model: db.models.user,
                    as: 'userGoing',
                    attributes: ['id', 'avatar']
                }, {
                    model: db.models.user,
                    as: 'userLike',
                    attributes: ['id', 'avatar']
                }]
            }).then(function(event) {
                cb(null, event);
            }).catch(cb);
        },
        function(event, cb) {
            userFriend.getFriendsIds(req.user.id, function(err, ids) {
                cb(err, req, req.user.id, event, ids);
            });
        },
        addGoingStatistics,
        function(event, cb) {
            event.canEdit = (req.user.id === event.userId);
            cb(null, event);
        }
    ], function(err, event) {
        callback(err, event);
    });
};

/**
 * @api {get} api/db/event/:id?access_token=? Request Event information
 * @apiName GetEventSearch
 * @apiGroup Event
 * @apiPermission Admin
 *
 * @apiUse EventParams
 *
 * @apiUse EventSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventSrv.searchEventsByParams = function(req, cb) {
    db.models.event.findAll({
        where: req.body
    }).complete(function(err, events) {
        cb(err, events);
    });
};

/**
 * @api {post} api/db/event?access_token=? Create new event
 * @apiDescription returns all model of created event
 * @apiName CreateEvent
 * @apiPermission UserSelf
 * @apiGroup Event
 * @apiUse EventParams
 * @apiUse EventSuccess
 * @apiError 400 JSON has information about error
 */
eventSrv.createNewEvent = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'event property is empty'
        });
    }
    if (req.body && req.user) {
        req.body.userId = req.body.userId || req.user.id;
    }
    db.models.event.create(req.body).then(function(event) {
        cb(null, event);
    }).catch(cb);
};
/**
 * @api {put} api/db/event/:eventId?access_token=? Update event info
 * @apiDescription Update event info by id
 * @apiName UpdateEvent
 * @apiPermission UserSelf
 * @apiGroup Event
 * @apiUse EventParams
 * @apiUse EventSuccess
 * @apiError 400 JSON has information about error
 */
eventSrv.updateEventById = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'event property is empty'
        });
    }
    db.models.event.find(req.params.id).complete(function(err, event) {
        if (err || !event) {
            return cb(err);
        }
        event.updateAttributes(req.body).complete(function(err,
            eventNew) {
            cb(err, eventNew);
        });
    });
};

/**
 * @api {delete} api/db/event/:id?access_token=? Delete event
 * @apiName DeleteEvent
 * @apiGroup Event
 * @apiPermission Admin
 *
 * @apiParam {UUID} id
 *
 * @apiError 400 JSON has information about error
 */
eventSrv.deleteEventById = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'event id is empty'
        });
    }
    db.models.event.find(req.params.eventId).complete(function(err, event) {
        if (err) {
            cb(err);
        } else {
            event.destroy().complete(function(err) {
                cb(err);
            });
        }
    });
};
