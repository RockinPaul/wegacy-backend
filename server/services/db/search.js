'use strict';
var db = require('../../models');
var async = require('async');

var search = module.exports;
var report = {};

function sendEachToSearch(model, cbModel) {
    model.findAll({}).complete(function (err, items) {
        report[model.name] = 0;
        if(err) {
            return cbModel(err);
        }
        async.eachLimit(items, 5, function (item, cb) {
                item.sendToSearch(function () {
                    console.info('index for ', item.id);
                    report[model.name] += 1;
                    cb();
                });
            },
            function (err) {
                cbModel(err, { report: 1});
            });
    });
}

search.reindex = function (req, callback) {
    var tmStart = Date.now();
    async.map(Object.keys(db.models), function (modelName, cbModel) {
        var model = db.models[modelName];
        if (model.options.instanceMethods &&
            model.options.instanceMethods.sendToSearch) {
            sendEachToSearch(model, function () {
                cbModel(null);
            });
        } else {
            cbModel(null);
        }
    }, function (err) {
        callback(err, {
            reindexCount: report,
            took: Date.now() - tmStart
        });
    });
};

