'use strict';
var _ = require('lodash');
var db = require('../../models');
var async = require('async');
var errorHandler = require('../../common/error-handler');

var eventComment = module.exports;

/**
 * @api {get} api/db/event/comment/:id Get comments of event
 * @apiDescription Result is {count: 2, rows: []}
 * @apiName EventGetComments
 * @apiGroup Event_Comment
 * @apiPermission User
 *
 * @apiParam {UUID} id eventId to comment
 * @apiParam {Number} offset Skip some comments (default: 0)
 * @apiParam {Number} limit Limit amount of comments (default: 100)
 *
 * @apiUse CommentSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventComment.getComment = function(req, callback) {
    if (!req.params && !req.params.id) {
        callback(errorHandler.errors.needEventId);
    }
    async.waterfall([

        function(cb) {
            db.models.event.find({
                where: {
                    id: req.params.id
                }
            }).complete(function(err, event) {
                if (!event) {
                    return cb(errorHandler.errors.eventNotFound());
                }
                cb(null);
            });
        },
        function(cb) {
            db.models.comment.findAndCountAll({
                where: {
                    eventId: req.params.id
                },
                limit: req.query.limit || 100,
                offset: req.query.offset || 0,
                include: [{
                    model: db.models.user,
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }, {
                    model: db.models.user,
                    as: 'replyToUser',
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }, {
                    model: db.models.user,
                    as: 'mentionUser',
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }],
                order: ['createdAt']
            }).complete(cb);
        },
    ], function(err, comments) {
        callback(err, comments);
    });
};
/**
 * @api {post} api/db/event/comment/:id Add comment to event
 * @apiName EventAddComment
 * @apiGroup Event_Comment
 * @apiPermission User
 *
 * @apiParam {UUID} id eventId to comment
 * @apiUse CommentParams
 *
 * @apiUse CommentSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventComment.addComment = function(req, callback) {
    var userId = req.query.userId || req.user.id;
    async.waterfall([

        function(cb) {
            db.models.event.find({
                where: {
                    id: req.params.id
                }
            }).then(function(event) {
                cb(null, event);
            }).catch(cb);
        },
        function(event, cb) {
            db.models.user.find({
                where: {
                    id: userId
                }
            }).then(function(user) {
                cb(null, user, event);
            }).catch(cb);
        },
        function(user, event, cb) {
            var commentBody = _.merge({
                userId: userId,
                replyTo: req.body.replyTo || null
            }, req.body);
            if (event && req.params.id !== 'match') {
                commentBody.eventId = event.id;
            } else {
                commentBody.matchId = user.matchId;
            }
            db.models.comment.create(commentBody)
                .then(function(comment) {
                    cb(null, comment);
                })
                .catch(cb);
        }
    ], function(err, comment) {
        callback(err, comment);
    });
};
/**
 * @api {delete} api/db/event/comment/:id Remove comment from event
 * @apiName EventRemoveComment
 * @apiGroup Event_Comment
 * @apiPermission Admin
 *
 * @apiParam {UUID} id commentId to remove comment
 *
 * @apiUse EventSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventComment.removeComment = function(req, callback) {
    async.waterfall([

        function(cb) {
            db.models.comment.destroy({
                where: {
                    id: req.params.id
                }
            }).complete(function(err) {
                cb(err);
            });
        }
    ], function(err) {
        callback(err);
    });
};
