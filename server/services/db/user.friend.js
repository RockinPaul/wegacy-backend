'use strict';
var async = require('async');
var db = require('../../models');
var userFriend = module.exports;

userFriend.getFriendsIds = function(userId, callback) {
    var needInclude = [];
    needInclude.push({
        model: db.models.user,
        as: 'userFollow',
        attributes: ['id']
    });
    async.waterfall(
        [
            function(cb) {
                db.models.user.find({
                    where: {
                        id: userId
                    },
                    include: needInclude
                }).complete(cb);
            },
            function(user, cb) {
                db.models.userFollow.findAll({
                    where: {
                        userFollowId: userId
                    }
                }).complete(function(err, usersFollowMe) {
                    var usersFollowMeIds = usersFollowMe.map(
                        function(follower) {
                            return follower.userId;
                        }
                    );
                    cb(err, user, usersFollowMeIds);
                });
            },
            function(user, usersFollowMeIds, cb) {
                var friendsId = [];
                user.userFollow.map(function(follow) {
                    if (usersFollowMeIds.indexOf(follow.id) !==
                        -1) {
                        friendsId.push(follow.id);
                    }
                });
                cb(null, friendsId);
            }
        ],
        function(err, friendsId) {
            callback(err, friendsId);
        }
    );
};
