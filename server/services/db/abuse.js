'use strict';
var db = require('../../models');
var _ = require('lodash');
var abuse = module.exports;

/**
 * @api {post} api/db/user/abuse/:id Abuse to user by id
 * @apiName CreateAbuse
 * @apiGroup User_Abuse
 * @apiPermission user
 *
 * @apiUse AbuseSuccess
 * @apiUse AbuseParam
 *
 * @apiError 400 JSON has information about error
 */
abuse.create = function(req, cb) {
    var body = _.merge(req.body, {
        userId: req.params.id
    });
    db.models.abuse.create(body).then(function(user){
        cb(null, user.going);
    }).catch(cb);
};
