'use strict';
var db = require('../../models');
var _ = require('lodash');
var errorSrv = module.exports;

/**
 * @api {post} api/db/user/error/:id Error from user by id
 * @apiName CreateError
 * @apiGroup User_Error
 * @apiPermission user
 *
 * @apiUse ErrorSuccess
 * @apiUse ErrorParam
 *
 * @apiError 400 JSON has information about error
 */
errorSrv.create = function(req, cb) {
    var body = _.merge(req.body, {
        userId: req.params.id
    });
    db.models.error.create(body).then(function(errorDb){
        cb(null, errorDb);
    }).catch(cb);
};

/**
 * @api {get} api/db/user/error Show all app errors
 * @apiName ShowErrors
 * @apiGroup User_Error
 * @apiPermission user
 *
 * @apiUse ErrorSuccess
 * @apiUse ErrorParam
 *
 * @apiError 400 JSON has information about error
 */
errorSrv.showAll = function(req, cb) {
    db.models.error.findAll({
        orderBy: [['createdAt', 'DESC']]
    }).then(function(errors){
        cb(null, errors);
    }).catch(cb);
};
