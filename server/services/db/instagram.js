'use strict';

var async = require('async');
var Instagram = require('instagram-node-lib');
var request = require('supertest');
var requestFile = require('request');
var fs = require('fs');
var temp = require('temp');
var path = require('path');

temp.track();

var config = require('../../config');
var db = require('../../models');
var logger = require('../../config/logger');

var instagram = module.exports;

instagram.getInstagram = function (req, callback) {
    callback(null, req.query['hub.challenge']);
};

var configInstagram = function () {
    Instagram.set('client_id', config.instagram.clientId);
    Instagram.set('client_secret', config.instagram.clientSecret);
    Instagram.set('callback_url', config.instagram.callbackUrl);
};

configInstagram();

instagram.getSubscriptions = function (req, callback) {
    Instagram.subscriptions.list({
        complete: function (data) {
            callback(null, data);
        }, error: callback
    });
};

instagram.unsubscribeAll = function (req, callback) {
    Instagram.subscriptions.unsubscribe_all({
        complete: function (data) {
            callback(null, data);
        }, error: callback
    });
};

instagram.subscribe = function (req, callback) {
    if (!req.body || !req.body.tag || !req.body.tag.length) {
        return callback({
            message: 'empty tag not allowed'
        });
    }
    db.models.subscription.find({
        where: {
            type: 'instagram', tag: req.body.tag,
        }
    }).then(function (subscription) {
        if (subscription) {
            return callback(null);
        } else {
            Instagram.subscriptions.subscribe({
                object: 'tag',
                object_id: req.body.tag,
                complete: function (data) {
                    db.models.subscription.findOrCreate({
                        where: {
                            type: 'instagram',
                            tag: req.body.tag,
                            verify_token: data.id
                        }
                    }).then(function (subscription) {
                        return callback(null, data, subscription);
                    }).catch(callback);
                },
                error: function () {
                    callback(null);
                }
            });
        }
    }).catch(callback);
};


var download = function (uri, filename, callback) {
    requestFile.head(uri, function (err, res, body) {
        //('content-type:', res.headers['content-type']);
        //('content-length:', res.headers['content-length']);
        requestFile(uri).pipe(fs.createWriteStream(filename)).on('close', function () {
            callback(res.headers, body);
        });
    });
};

var saveFile = function (instagramLink, photoUrl, instagramId, cb) {
    temp.mkdir('instagram', function (err, dirPath) {
        var tmpFileName = path.join(dirPath, instagramId + path.extname(photoUrl));
        download(photoUrl, tmpFileName, function () {
            request(global.url || config.url).post('api/file/instagram/?access_token=777').attach('image', tmpFileName).end(function (err, res) {
                if (res && res.body) {
                    cb(err, res.body.image);
                } else {
                    cb(err);
                }
            });
        });
    });
};

var haveActiveEventsWithHashTag = function (hashTag, cb) {
    db.models.event.count({
        where: {
            hashTag: hashTag,
            endDateTime: {
                gte: db.sequelize.fn('NOW')
            }
        }
    }).then(function (eventsCount) {
        cb(null, eventsCount);
    }).catch(cb);
};

var unsubscribeById = function (subscribeId, callback) {
    Instagram.subscriptions.unsubscribe({
        id: subscribeId, complete: function (data) {
            logger.trace('unsubscribed by tag ' + subscribeId, data);
            return callback(null, data);
        }, error: function () {
            callback(null);
        }
    });
};

var unsubscribeInstagram = function (hashTag, callback) {
    Instagram.subscriptions.list({
        complete: function (data) {
            async.each(data, function (subscription, cb) {
                if (subscription.object_id == hashTag) {
                    unsubscribeById(subscription.id, cb);
                } else {
                    cb(null);
                }
            }, callback);
        }, error: callback
    });
};

var savePhotoIfNew = function (hashTag, instagramLink, photoUrl, instagramId, created_time, cbEach) {
    db.models.photo.count({
        where: {
            hashTag: hashTag
        }
    }).then(function(photosCount){
        if(photosCount > 300){
            unsubscribeInstagram(hashTag, function(){
                return ;
            })
        }
    });
    haveActiveEventsWithHashTag(hashTag, function (err, eventsCnt) {
        if (!err && eventsCnt > 0) {
            db.models.photo.find({
                where: {
                    hashTag: hashTag, externalId: instagramId,
                }
            }).then(function (photo) {
                if (photo) {
                    cbEach();
                } else {
                    async.waterfall([function (cb) {
                        saveFile(instagramLink, photoUrl, instagramId, cb);
                    }, function (photoId, cb) {
                        db.models.photo.create({
                            urlId: photoId,
                            hashTag: hashTag,
                            externalId: instagramId,
                            externalUrl: instagramLink,
                            externalCreatedTime: created_time
                        }).then(function () {
                            cb(null);
                        }).catch(cbEach);
                    }], function (err) {
                        cbEach(err);
                    });
                }

            }).catch(cbEach);
        } else {
            unsubscribeInstagram(hashTag, function () {
                logger.trace('event expire for photo for hash ' + hashTag);
                cbEach('all events expired');
            });
        }
    });
};

var getInstagramImagesByTag = function (hashTag, cb) {
    Instagram.tags.recent({
        name: hashTag, complete: function (data) {
            cb(null, data, hashTag);
        }, error: cb
    });

};

var savePhotoByInstagramInfo = function (images, hashTag, cb) {
    async.eachLimit(images, 5, function (image, cbEach) {
        savePhotoIfNew(hashTag, image.link, image.images.low_resolution.url, image.id, image.created_time, cbEach);
    }, function () {
        cb(null);
    });
};

var getEventById = function (eventId) {
    return function (cb) {
        db.models.event.find({
            where: {
                id: eventId
            }
        }).then(function (event) {
            cb(null, event.hashTag);
        }).catch(cb);
    };
};
instagram.refreshPhotos = function (req, callback) {
    async.waterfall([getEventById(req.params.eventId), getInstagramImagesByTag, savePhotoByInstagramInfo], function (err) {
        callback(err, {});
    });
};

var refreshPhotosByHashTag = function (hashTag, callback) {
    async.waterfall([function (cb) {
        return cb(null, hashTag);
    }, getInstagramImagesByTag, savePhotoByInstagramInfo], function (err) {
        callback(err, {});
    });
};

instagram.postInstagram = function (req, callback) {
    async.each(req.body, function (data, cb) {
        logger.data('postInstagram object_id', data.object_id);
        logger.data('postInstagram subscription_id', data.subscription_id);
        refreshPhotosByHashTag(data.object_id, function () {
            cb();
        });
    }, function () {
        callback(null, {});
    });
};
