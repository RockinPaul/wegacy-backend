'use strict';

var db = require('../../models');
var logger = require('../../config/logger');
var async = require('async');

var user = {};

user.test = function(query, cb) {
    if (!global.cnt) {
        global.cnt = 0;
    }

    cb(null, {
        answer: global.cnt++
    });
};

/**
 * @api {get} api/db/users Request All Users
 * @apiName GetUsers
 * @apiGroup User
 * @apiPermission admin
 * @apiDescription Returns array of users {count: 2, rows: [i]}
 *
 * @apiParam {Number} offset Skip some users (default: 0)
 * @apiParam {Number} limit Limit amount of users (default: 100)
 * @apiParam {Bool} all If need all associated info about user (default: false)
 *
 * @apiUse UserSuccess
 * @apiError 400 JSON has information about error
 */
user.getAllUsers = function(req, cb) {
    db.models.user.findAndCountAll({
        limit: req.query.limit || 100,
        offset: req.query.offset || 0,
        include: req.query.all ? [{
            all: true
        }] : undefined
    }).complete(function(err, users) {
        if (err) {
            logger.trace('getAllUsers', err);
        }
        cb(err, users);
    });
};

user.getUserInvites = function(req, cb) {
    db.models.eventInvite.findAll({
        where: {
            toUserId: req.params.id
        }
    }).complete(function(err, invites) {
        if (err) {
            logger.trace('getAllUsers', err);
        }
        cb(err, invites);
    });
};

var getUserStats = function(err, userResult, req, callback) {
    if (typeof userResult.toJSON === 'function') {
        userResult = userResult.toJSON();
    }
    userResult.stats = {};
    if (userResult.events) {
        userResult.stats.eventsCount = userResult.events.length;
        delete userResult.events;
    }
    if (userResult.userFollow) {
        userResult.stats.followersCount = userResult.userFollow.length;
    }
    var isIamFollowUser = false;
    userResult.userFollow.map(function(
        theUser) {
        if (theUser.id === req.user.id) {
            isIamFollowUser = true;
        }
        return {
            id: theUser.userId,
            userFollowing: theUser
        };
    });
    userResult.stats.isIamFollowUser = isIamFollowUser;
    db.models.userFollow.findAll({
        where: {
            userFollowId: userResult.id
        }
    }).then(function(following) {
        var isUserFollowMe = false;
        userResult.userFollowing = following.map(function(
            theUser) {
            if (theUser.userId === req.user.id) {
                isUserFollowMe = true;
            }
            return {
                id: theUser.userId,
                userFollowing: theUser
            };
        });
        userResult.stats.isUserFollowMe = isUserFollowMe;
        userResult.stats.followingCount = userResult.userFollowing.length;
        callback(err, userResult);
    }).catch(callback);
};

var getUserInfo = function(userId, req, callback) {
    var needInclude = [];
    if (req.params.all) {
        needInclude.push({
            all: true
        });
    }
    needInclude.push({
        model: db.models.user,
        as: 'userFollow',
        attributes: ['id']
    });
    needInclude.push({
        model: db.models.category,
        attributes: ['id', 'title']
    });
    needInclude.push({
        model: db.models.event
    });
    async.waterfall([

            function(cb) {
                db.models.user.find({
                    where: {
                        id: userId
                    },
                    include: needInclude
                }).complete(cb);
            }
        ],
        function(err, user) {
            getUserStats(err, user, req, callback);
        });
};

/**
 * @api {get} api/db/user/follow?access_token get follow of the user
 * @apiName GetFollow
 * @apiGroup User_Follow
 * @apiPermission User
 *
 * @apiParam {Guid} userId Optional param if need events of other user
 * @apiParam {String} access_token
 * @apiParam {Number} offset Skip some users (default: 0)
 * @apiParam {Number} limit Limit amount of users (default: 100)
 *
 * @apiUse UserSuccess
 *
 * @apiError 400 JSON has information about error
 */
user.getFollow = function(req, callback) {
    db.models.userFollow.findAndCountAll({
        where: db.Sequelize.and([{
            userId: req.query.userId || req.user.id
        }]),
        limit: req.query.limit || 100,
        offset: req.query.offset || 0
    }).then(function(userFollows) {
        var rows = [];
        async.eachLimit(userFollows.rows, 10, function(userFollow, cb) {
            getUserInfo(userFollow.userFollowId, req, function(err, user) {
                rows.push(user);
                cb(err);
            });
        }, function(err) {
            userFollows.rows = rows;
            callback(err, userFollows);
        });
    });
};

/**
 * @api {get} api/db/user/following?access_token get following of the user
 * @apiName GetFollowing
 * @apiGroup User_Follow
 * @apiPermission User
 *
 * @apiParam {Guid} userId Optional param if need events of other user
 * @apiParam {String} access_token
 * @apiParam {Number} offset Skip some users (default: 0)
 * @apiParam {Number} limit Limit amount of users (default: 100)
 *
 *
 * @apiError 400 JSON has information about error
 */
user.getFollowing = function(req, callback) {
    db.models.userFollow.findAndCountAll({
        where: db.Sequelize.and([{
            userFollowId: req.query.userId || req.user.id
        }]),
        limit: req.query.limit || 100,
        offset: req.query.offset || 0
    }).then(function(userFollows) {
        var rows = [];
        async.eachLimit(userFollows.rows, 10, function(userFollow, cb) {
            getUserInfo(userFollow.userId, req, function(err, user) {
                rows.push(user);
                cb(err);
            });
        }, function(err) {
            userFollows.rows = rows;
            callback(err, userFollows);
        });
    });
};

/**
 * @api {get} api/db/event/:eventId/going?access_token get going users by eventId
 * @apiName GetGoingByEventId
 * @apiGroup Event_Going
 * @apiPermission User
 *
 * @apiParam {String} access_token
 * @apiParam {Guid} eventId
 * @apiParam {Number} offset Skip some users (default: 0)
 * @apiParam {Number} limit Limit amount of users (default: 100)
 *
 * @apiUse UserSuccess
 *
 * @apiError 400 JSON has information about error
 */
user.getGoingUsersByEvent = function(req, callback) {
    db.models.userGoing.findAndCountAll({
        where: db.Sequelize.and([{
            eventId: req.params.eventId
        }]),
        limit: req.query.limit || 100,
        offset: req.query.offset || 0
    }).then(function(userFollows) {
        var rows = [];
        async.eachLimit(userFollows.rows, 10, function(userFollow, cb) {
            getUserInfo(userFollow.userId, req, function(err, user) {
                rows.push(user);
                cb(err);
            });
        }, function(err) {
            userFollows.rows = rows;
            callback(err, userFollows);
        });
    });
};


/**
 * @api {get} api/db/user/:id Request User information by Id
 * @apiName GetUser
 * @apiGroup User
 * @apiPermission Admin or UserSelf
 *
 * @apiParam {UUID} id
 * @apiParam {Bool} all If need all associated info about user (default: false)
 *
 * @apiUse UserSuccess
 * @apiSuccess {Integer} stats/eventsCount
 * @apiSuccess {Integer} stats/followersCount
 * @apiSuccess {Integer} stats/followingCount
 *
 * @apiError 400 JSON has information about error
 */
user.getUserById = function(req, callback) {
    getUserInfo(req.params.id, req, callback);
};

/**
 * @api {get} api/db/user Request User self information by token
 * @apiName GetUserSelf
 * @apiGroup User
 * @apiPermission Admin or UserSelf
 * @apiDescription Returns user info and categories and userFollow
 *
 * @apiParam {UUID} id
 * @apiParam {Bool} all If need all associated info about user (default: false)
 *
 * @apiUse UserSuccess
 * @apiSuccess {Integer} stats/eventsCount
 * @apiSuccess {Integer} stats/followersCount
 * @apiSuccess {Integer} stats/followingCount
 *
 * @apiError 400 JSON has information about error
 */
user.getUserSelfByToken = function(req, callback) {
    getUserInfo(req.user.id, req, callback);
};


/**
 * @api {get} api/db/user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 * @apiPermission Admin
 *
 * @apiUse UserParams
 *
 * @apiUse UserSuccess
 *
 * @apiError 400 JSON has information about error
 */
user.searchUsersByParams = function(req, cb) {
    db.models.user.findAll({
        where: req.body
    }).complete(function(err, users) {
        if (err) {
            logger.trace('searchUsersByParams', err);
        }
        cb(err, users);
    });
};

/**
 * @api {post} api/db/user Create new user
 * @apiDescription returns all model of created user
 * @apiName CreateUser
 * @apiPermission UserSelf
 * @apiGroup User
 * @apiUse UserParams
 * @apiUse UserSuccess
 * @apiSuccess {Object} oauth info with tokens { token_type: 'bearer',
     access_token: '07cc45565da812933c060d82364c0fe262072dcc',
     expires_in: 3600,
     refresh_token: '0e80df06052ba292711eb2564dbc43b8190832bb' } }
 * @apiError 400 JSON has information about error
 */
user.createNewUser = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'user property is empty'
        });
    }
    db.models.user.create(req.body).complete(function(err, user) {
        if (err) {
            logger.trace('searchUsersByParams', err);
        }
        logger.trace('create new user', user ? user.toJSON() : '');
        cb(err, user);
    });
};

/**
 * @api {put} api/db/user/:id Update user info
 * @apiDescription Update user info by id
 * @apiName UpdateUser
 * @apiPermission UserSelf
 * @apiGroup User
 * @apiUse UserParams
 * @apiUse UserSuccess
 * @apiError 400 JSON has information about error
 */

user.updateUserById = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'user property is empty'
        });
    }
    db.models.user.find(req.params.id).complete(function(err, user) {
        if (err || !user) {
            if (err) {
                logger.trace('updateUserById not found', err);
            }
            return cb(err);
        }
        user.updateAttributes(req.body).complete(function(err,
            userNew) {
            if (err) {
                logger.trace('updateUserById cant update',
                    err);
            }
            cb(err, userNew);
        });
    });
};
/**
 * @api {put} api/db/user Update user self info
 * @apiDescription Update user self info by token
 * @apiName UpdateUserSelf
 * @apiPermission UserSelf
 * @apiGroup User
 * @apiUse UserParams
 * @apiUse UserSuccess
 * @apiError 400 JSON has information about error
 */
user.updateUserSelfByToken = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'user property is empty'
        });
    }
    db.models.user.find(req.user.id).complete(function(err, user) {
        if (err || !user) {
            if (err) {
                logger.trace('updateUserById not found', err);
            }
            return cb(err);
        }
        user.updateAttributes(req.body).complete(function(err,
            userNew) {
            if (err) {
                logger.trace('updateUserById cant update',
                    err);
            }
            cb(err, userNew);
        });
    });
};
/**
 * @api {put} api/db/user/password Update user password
 * @apiDescription Update user password by token
 * @apiName UpdateUserPasswordSelf
 * @apiPermission UserSelf
 * @apiGroup User
 * @apiSuccess {String} old hash of old password
 * @apiSuccess {String} new hash of new password
 * @apiUse UserSuccess
 * @apiError 400 JSON has information about error
 */
user.updateUserPassword = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'user property is empty'
        });
    }
    var password = null;
    if (req.body.old && req.body.old != '' && req.body.old.indexOf('null') == -1) {
        password = req.body.old;
    }
    db.models.user.find({
        where: {
            id: req.user.id,
            password: password
        }
    }).complete(function(err, user) {
        if (err || !user) {
            if (err) {
                logger.trace('updateUserById not found', err);
                return cb(err);
            } else {
                return cb(404);
            }
        }
        user.updateAttributes({
            password: req.body.new
        }).complete(function(err,
            userNew) {
            if (err) {
                logger.trace('updateUserById cant update',
                    err);
            }
            cb(err, userNew);
        });
    });
};

/**
 * @api {post} api/db/user/social/:socialType Create new user by social
 * @apiDescription Create user by social network info (facebook or vk)
 * @apiName CreateUserBySocial
 * @apiPermission UserSelf
 * @apiGroup User
 * @apiParam {Enum} socialType 'facebook' or 'vk'
 * @apiParam {String} id Social id, need to be the same to login later
 * @apiParam {Object} info Social info, object with info from social network with first_name, last_name...
 * @apiUse UserSuccess
 * @apiSuccess {Object} oauth info with tokens { token_type: 'bearer',
     access_token: '07cc45565da812933c060d82364c0fe262072dcc',
     expires_in: 3600,
     refresh_token: '0e80df06052ba292711eb2564dbc43b8190832bb' } }
 * @apiError 400 JSON has information about error
 */
user.createBySocialNetwork = function(req, callback) {
    logger.trace('start create user by social');
    async.waterfall([

        function(cb) {
            var info = {};
            var err = null;
            if (req.params.socialType === 'facebook') {
                info.socialType = 'facebook';
                info.email = req.body.info.email;
                info.firstName = req.body.info.first_name;
                info.lastName = req.body.info.last_name;
                info.socialId = req.body.id;
            } else if (req.params.socialType === 'vk') {
                info.socialType = 'vk';
                info.firstName = req.body.info.first_name;
                info.lastName = req.body.info.last_name;
                info.email = req.body.info.email;
                info.socialId = req.body.id;
            } else {
                err = "Unknown social network " + req.params.socialType;
            }
            cb(err, info);
        },
        function(info, cb) {
            db.models.user.find({
                where: {
                    email: info.email
                }
            }).then(function(user) {
                cb(null, user, info);
            }).catch(cb);
        },
        function(user, info, cb) {
            logger.trace('start add user by social');
            if(user){
                logger.trace('user exists for social');
                return cb(null, user, info);
            }
            db.models.user.create({
                firstName: info.firstName,
                lastName: info.lastName,
                email: info.email,
                status: 'user'
            }).complete(function(err, user) {
                if (err) {
                    logger.trace(
                        'cant create user by social',
                        err);
                }
                cb(err, user, info);
            });
        },
        function(user, info, cb) {
            db.models.social.create({
                socialId: info.socialId,
                socialType: info.socialType,
                userId: user.id,
                info: JSON.stringify(req.body.info)
            }).complete(function(err, social) {
                if (err) {
                    logger.trace('cant create by social',
                        err);
                }
                logger.trace(
                    'Created new social for user by social',
                    social ? social.toJSON() : '');
                cb(err, user);
            });
        }
    ], function(err, user) {
        callback(err, user);
    });
};

/**
 * @api {delete} api/db/user/:id Delete user
 * @apiName DeleteUser
 * @apiGroup User
 * @apiPermission Admin
 *
 * @apiParam {UUID} id
 *
 * @apiError 400 JSON has information about error
 */
user.deleteUserById = function(req, cb) {
    if (!req.body) {
        cb({
            err: 'user id is empty'
        });
    }
    db.models.user.find(req.body.id).complete(function(err, user) {
        if (err) {
            if (err) {
                logger.trace('cant find user for delete', err);
            }
            cb(err);
        } else {
            db.models.social.destroy({
                where: {
                    userId: user.id
                }
            }).complete(function(err) {
                if (err) {
                    return cb(err);
                }
                user.destroy().complete(function(err) {
                    cb(err);
                });
            });
        }
    });
};
module.exports = user;
