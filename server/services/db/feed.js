'use strict';

var db = require('../../models');
var logger = require('../../config/logger');
var async = require('async');
var _ = require('lodash');

var feed = module.exports;

function getUserFriends(userId, callback) {
    async.waterfall([
        function(cb) {
            var rpc = require('../../common/amqp/amqp-rpc');
            rpc.call('get/db/user/:userId/friends', {
                params: {
                    userId: userId
                }
            }, function(err, friendsIds) {
                cb(err, friendsIds);
            });
        },
        function(friendsIds, cb) {
            db.models.user.findAll({
                where: {
                    id: { in : friendsIds
                    }
                }
            }).then(function(friends) {
                cb(null, friends);
            });
        }
    ], callback);
}

var getUser = function(userId) {
    return function(cbF) {
        db.models.user.find({
            where: {
                id: userId
            }
        }).then(function(user) {
            cbF(null, user.toJSON());
        });
    };
};
var getEvent = function(eventId) {
    return function(cbF) {
        db.models.event.find({
            where: {
                id: eventId
            }
        }).then(function(event) {
            cbF(null, event);
        });
    };
};

feed.newEvent = function(req, callback) {
    getUserFriends(req.body.userId, function(err, friends) {
        async.eachLimit(friends, 20, function(friend, cb) {
            db.models.feed.create({
                fromUserId: req.body.userId,
                toUserId: friend.id,
                sourceId: req.body.id,
                type: 'newEvent'
            }).then(function(feedEvent) {
                callback(null, feedEvent);
            }).catch(cb);
        }, callback);
    });
};

feed.newLike = function(req, callback) {
    async.waterfall([
        getEvent(req.body.eventId),
        function(event, cb) {
            db.models.feed.create({
                sourceId: req.body.id,
                fromUserId: req.body.userId,
                toUserId: event.userId,
                type: 'newLike'
            }).then(function(feedEvent) {
                cb(null, feedEvent);
            }).catch(cb);
        }
    ], function(err, feedEvent) {
        callback(err, feedEvent);
    });
};

feed.newComment = function(req, callback) {
    async.waterfall([
        getEvent(req.body.eventId),
        function(event, cb) {
            db.models.feed.create({
                sourceId: req.body.id,
                fromUserId: req.body.userId,
                toUserId: event.userId,
                type: 'newComment'
            }).then(function(feedEvent) {
                cb(null, feedEvent);
            }).catch(cb);
        }
    ], function(err, feedEvent) {
        callback(err, feedEvent);
    });
};

feed.newInvite = function(req, callback) {
    db.models.feed.create({
        sourceId: req.body.id,
        fromUserId: req.body.userId,
        toUserId: req.body.toUserId,
        type: 'newInvite'
    }).then(function(feedEvent) {
        callback(null, feedEvent);
    }).catch(callback);
};

feed.newFollow = function(req, callback) {
    db.models.feed.create({
        sourceId: req.body.id,
        fromUserId: req.body.userId,
        toUserId: req.body.userFollowId,
        type: 'newFollow'
    }).then(function(feedEvent) {
        callback(null, feedEvent);
    }).catch(callback);
};

feed.newGoing = function(req, callback) {
    async.waterfall([
        getEvent(req.body.eventId),
        function(event, cb) {
            db.models.feed.create({
                sourceId: req.body.id,
                fromUserId: req.body.userId,
                toUserId: event.userId,
                type: 'newGoing'
            }).then(function(feedEvent) {
                cb(null, feedEvent);
            }).catch(cb);
        }
    ], function(err, feedEvent) {
        callback(err, feedEvent);
    });
};

var getFn = {};


getFn.newEvent = function(feed, callback) {
    async.waterfall([
        function(cb) {
            db.models.event.find({
                id: feed.sourceId
            }).then(function(event) {
                cb(null, event);
            }).catch(cb);
        },
        function(event, cb) {
            if(!event) {
                return cb(null, null);
            }
            async.parallel({
                fromUser: getUser(feed.fromUserId)
            }, function(err, result) {
                if((!result || !result.fromUser || !event) ){
                    return cb(null, null);
                }
                cb(err, _.merge(feed, result, {
                    event: event
                }));
            });
        }
    ], callback);
};

getFn.newLike = function(feed, callback) {
    async.waterfall([
        function(cb) {
            db.models.userLike.find({
                where: {
                    id: feed.sourceId
                }
            }).then(function(userLike) {
                cb(null, userLike);
            }).catch(cb);
        },
        function(userLike, cb) {
            if(!userLike) {
                return cb(null, null);
            }
            async.parallel({
                fromUser: getUser(userLike.userId),
                event: getEvent(userLike.eventId)
            }, function(err, result) {
                if((!result || !result.fromUser || !result.event) ){
                    return cb(null, null);
                }
                cb(err, _.merge(feed, result));
            });
        }
    ], callback);
};

getFn.newInvite = function(feed, callback) {
    async.waterfall([
        function(cb) {
            db.models.eventInvite.find({
                where: {
                    id: feed.sourceId
                }
            }).then(function(eventInvite) {
                cb(null, eventInvite);
            }).catch(cb);
        },
        function(eventInvite, cb) {
            if(!eventInvite) {
                return cb(null, null);
            }
            async.parallel({
                fromUser: getUser(eventInvite.userId),
                event: getEvent(eventInvite.eventId)
            }, function(err, result) {
                if((!result || !result.fromUser || !result.event) ){
                    return cb(null, null);
                }
                cb(err, _.merge(feed, result));
            });
        }
    ], callback);
};

getFn.newGoing = function(feed, callback) {
    async.waterfall([
        function(cb) {
            db.models.userGoing.find({
                where: {
                    id: feed.sourceId
                }
            }).then(function(eventInvite) {
                cb(null, eventInvite);
            }).catch(cb);
        },
        function(userGoing, cb) {
            if(!userGoing) {
                return cb(null, null);
            }
            async.parallel({
                fromUser: getUser(userGoing.userId),
                event: getEvent(userGoing.eventId)
            }, function(err, result) {
                if((!result || !result.fromUser || !result.event) ){
                    return cb(null, null);
                }
                cb(err, _.merge(feed, result));
            });
        }
    ], callback);
};

getFn.newComment = function(feed, callback) {
    async.waterfall([
        function(cb) {
            db.models.comment.find({
                where: {
                    id: feed.sourceId,
                },
                include: [{
                    model: db.models.user,
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                },{
                    model: db.models.user,
                    as: 'replyToUser',
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                },{
                    model: db.models.user,
                    as: 'mentionUser',
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }]
            }).then(function(comment) {
                cb(null, comment);
            }).catch(cb);
        },
        function(comment, cb) {
            if(!comment) {
                return cb(null, null);
            }
            async.parallel({
                fromUser: getUser(comment.userId),
                event: getEvent(comment.eventId),
                comment: function(cbText) {
                    cbText(null, comment);
                }
            }, function(err, result) {
                if((!result || !result.fromUser || !result.event || !result.comment) ){
                    return cb(null, null);
                }
                cb(err, _.merge(feed, result));
            });
        }
    ], callback);
};

getFn.newFollow = function(feed, callback) {
    async.waterfall([
        function(cb) {
            db.models.userFollow.find({
                where: {
                    id: feed.sourceId
                }
            }).then(function(userFollow) {
                cb(null, userFollow);
            }).catch(cb);
        },
        function(userFollow, cb) {
            if(!userFollow) {
                return cb(null, null);
            }
            async.parallel({
                fromUser: getUser(userFollow.userId)
            }, function(err, user) {
                if(!user){
                    return cb(null, null);
                }
                cb(err, _.merge(feed, user));
            });
        }
    ], callback);
};


var getFeedByType = function(feed, cbFeed) {
    if (!getFn[feed.type]) {
        logger.error('dont know feed type ' + feed.type);
        return cbFeed('dont know feed type ' + feed.type);
    }
    getFn[feed.type](feed, cbFeed);
};

/**
 * @api {get} api/db/activity Get feed of activity
 * @apiName FeedActivity
 * @apiGroup Activity
 * @apiPermission User
 *
 * @apiParam {Number} offset Skip some activity (default: 0)
 * @apiParam {Number} limit Limit amount of activity (default: 100)
 *
 * @apiSuccess {Guid} id id of the feed
 * @apiSuccess {Enum} type type of the feed: newFollow, newEvent, newLike, newInvite, newGoing, newComment
 * @apiSuccess {DateTime} createdAt
 * @apiSuccess {DateTime} updatedAt
 * @apiSuccess {Guid} fromUserId
 * @apiSuccess {Guid} toUserId
 * @apiSuccess {Guid} sourceId id of entity of feed
 * @apiSuccess {Object} fromUser all info about fromUser
 * @apiSuccess {Object} event all info about event
 * @apiSuccess {Object} comment comment for newComment
 *
 * @apiError 400 JSON has information about error
 */
feed.getFeed = function(req, callback) {
    async.waterfall([function(cb) {
        db.models.feed.findAndCount({
            where: {
                toUserId: req.user.id
            },
            limit: req.query.limit || 100,
            offset: req.query.offset || 0,
            order: [
                ['updatedAt', 'DESC']
            ]
        }).then(function(feeds) {
            cb(null, feeds);
        }).catch(callback);
    }, function(feeds, cb) {
        async.map(feeds.rows, function(feed, cbFeed) {
            feed = feed.toJSON();
            getFeedByType(feed, cbFeed);
        }, function(err, results) {
            results = results.filter(function(result){
                return result !== null;
            });
            feeds.rows = results;
            cb(null, feeds);
        });
    }], callback);
};
