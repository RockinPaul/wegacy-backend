'use strict';
var async = require('async');
var db = require('../../models');
var userFriend = require('./user.friend');
var eventInvite = module.exports;

/**
 * @api {post} api/db/event/:eventId/invite/:userId?access_token add invite to user
 * @apiName AddInvite
 * @apiGroup User_Invite
 * @apiPermission User
 *
 * @apiParam {UUID} eventId
 * @apiParam {UUID} userId
 * @apiParam {String} access_token
 *
 * @apiError 400 JSON has information about error
 */
eventInvite.addInvite = function(req, callback) {
    if (!req.params && !req.params.id) {
        callback({
            err: 'follow id is empty'
        });
    }
    async.waterfall([
        function(cb) {
            db.models.user.find({
                where: {
                    id: req.user.id
                }
            }).complete(cb);
        },
        function(user, cb) {
            db.models.user.find({
                where: {
                    id: req.params.userId
                }
            }).then(function(inviteUser) {
                cb(null, user, inviteUser);
            }).catch(cb);
        },
        function(user, inviteUser, cb) {
            db.models.event.find({
                where: {
                    id: req.params.eventId
                }
            }).then(function(event) {
                cb(null, user, inviteUser, event);
            }).catch(cb);
        },
        function(user, inviteUser, event, cb) {
            db.models.eventInvite.create({
                eventId: event.id,
                userId: user.id,
                toUserId: inviteUser.id
            }).then(function(invite) {
                cb(null, invite);
            }).catch(cb);
        }
    ], function(err) {
        callback(err, {});
    });
};

/**
 * @api {post} api/db/event/:eventId/invite?access_token add invite to all friends
 * @apiName AddInviteToAllFriends
 * @apiGroup User_Invite
 * @apiPermission User
 *
 * @apiParam {UUID} eventId
 * @apiParam {String} access_token
 * @apiParam {Array} ids (optional) id of users to invite (in BODY of POST)
 * @apiSuccess {Array} ids of invited users
 *
 * @apiError 400 JSON has information about error
 */
eventInvite.addInviteToAllFriends = function(req, callback) {
    if (!req.params && !req.params.id) {
        callback({
            err: 'follow id is empty'
        });
    }
    async.waterfall([
        function(cb) {
            db.models.user.find({
                where: {
                    id: req.user.id
                }
            }).complete(cb);
        },
        function(user, cb) {
            if (req.body && req.body.ids) {
                cb(null, user, req.body.ids);
            } else {
                userFriend.getFriendsIds(user.id, function(err, ids) {
                    cb(err, user, ids);
                });
            }
        },
        function(user, inviteFriends, cb) {
            db.models.event.find({
                where: {
                    id: req.params.eventId
                }
            }).then(function(event) {
                cb(null, user, inviteFriends, event);
            }).catch(cb);
        },
        function(user, inviteFriends, event, cbFriend) {
            async.eachLimit(inviteFriends, 10, function(friendId,
                cb) {
                db.models.eventInvite.findOrCreate({
                    where: {
                        eventId: event.id,
                        userId: user.id,
                        toUserId: friendId
                    }
                }).then(function(invite) {
                    cb(null, invite);
                }).catch(cb);
            }, function() {
                return cbFriend(null, inviteFriends);
            });
        }
    ], function(err, inviteFriends) {
        callback(err, inviteFriends);
    });
};
