'use strict';
var category = module.exports;
var db = require('../../models');
var logger = require('../../config/logger');

/**
 * @api {get} api/db/user/categories/list All available categories
 * @apiName GetUserCategoriesList
 * @apiDescription All categories list with title and id
 * @apiGroup User_Category
 * @apiPermission User
 * @apiSuccess {String} id
 * @apiSuccess {String} title i.e. 'Sport', 'Business'...
 *
 * @apiError 400 JSON has information about error
 */
category.getUserCategoriesList = function(req, cb) {
    db.models.category.findAll({})
        .complete(function(err, categories) {
            if (err || !categories) {
                return cb(404, 'categories not found');
            }
            cb(err, categories.map(function(el) {
                return {
                    id: el.id,
                    title: el.title
                };
            }));
        });
};

/**
 * @api {get} api/db/user/categories Request User categories
 * @apiName GetUserCategories
 * @apiDescription Returns array of selected categories. Category of events that the user likes.
 * @apiGroup User_Category
 * @apiPermission User
 * @apiSuccess {String} id
 * @apiSuccess {String} title i.e. 'Sport', 'Business'...
 *
 * @apiError 400 JSON has information about error
 */
category.getUserCategories = function(req, cb) {
    logger.trace('CATEGORY', req);
    db.models.user.find({
        where: {
            id: req.user.id
        },
        include: db.models.category
    }).complete(function(err, user) {
        if (err || !user) {
            logger.error(err, user);
            return cb(404, 'user not found');
        }
        cb(err, user.categories.map(function(el) {
            return {
                id: el.id,
                title: el.title
            };
        }));
    });
};

/**
 * @api {put} api/db/user/categories Save User categories
 * @apiName SaveUserCategories
 * @apiDescription Returns array of selected categories. Category of events that the user likes.
 * @apiGroup User_Category
 * @apiPermission User
 * @apiParam {Array} categories i.e. ['Sport', 'Business']
 * @apiSuccess {String} id
 * @apiSuccess {String} title i.e. 'Sport', 'Business'...
 *
 * @apiError 400 JSON has information about error
 */
category.putUserCategories = function(req, cb) {
    var userId = req.query.userId || req.user.id;
    db.models.user.find({
        where: {
            id: userId
        }
    }).complete(function(err, user) {
        if (err || !user) {
            return cb(404, 'user not found');
        }
        db.models.category.findAll({
            where: {
                title: req.body.categories
            }
        }).complete(function(err, categories) {
            user.setCategories(categories).complete(function(err) {
                cb(err, categories);
            });
        });
    });
};
