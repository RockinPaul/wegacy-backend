'use strict';
var db = require('../../models');
var going = module.exports;

/**
 * @api {get} api/db/user/going Request All Events to going
 * @apiName GetUserGoingEventList
 * @apiGroup User_Going
 * @apiPermission user
 * @apiDescription Returns array of events the user going to go
 *
 * @apiParam {Guid} userId Optional param if need events of other user
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 *
 * @apiUse EventSuccess
 * @apiError 400 JSON has information about error
 */
going.getUserGoing = function(req, cb) {
    db.models.user.find({
        where: {
            id: req.query.userId || req.user.id
        },
        limit: req.query.limit || 100,
        offset: req.query.offset || 0,
        include: [
            { model: db.models.event, as: 'going' }
        ]
    }).complete(function(err, user){
        cb(null, user.going);
    });
};
/**
 * @api {put} api/db/user/going/:eventId Add user to Event for going
 * @apiName PutUserGoingToEvent
 * @apiGroup User_Going
 * @apiPermission user
 * @apiDescription Returns event the user going to go
 *
 * @apiParam {Guid} eventId id of event, where user going to go
 *
 * @apiUse EventSuccess
 * @apiError 400 JSON has information about error
 */
going.addUserGoing = function(req, cb) {
    db.models.user.find({
        where: {
            id: req.user.id
        }
    }).complete(function(err, user){
        db.models.event.find({
            where: {
                id: req.params.eventId
            }
        }).complete(function(err, event){
            user.addGoing(event).complete(function(){
                event.sendToSearch(function(){
                    cb(null, event);
                });
            });
        });
    });
};
/**
 * @api {delete} api/db/user/going/:eventId Delete user from Event for going
 * @apiName DeleteUserGoingToEvent
 * @apiGroup User_Going
 * @apiPermission user
 * @apiDescription Delete user from going to event
 *
 * @apiParam {Guid} eventId id of event, where user going to go
 *
 * @apiError 400 JSON has information about error
 */
going.deleteUserGoing = function(req, cb) {
    db.models.userGoing.find({
        where:
            db.Sequelize.and([
                { userId: req.user.id },
                { eventId: req.params.eventId }
            ])
    }).complete(function(err, going){
        going.destroy().complete(function(err){
            cb(err, {});
        });
    });
};
