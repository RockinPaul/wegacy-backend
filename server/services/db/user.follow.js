'use strict';
var async = require('async');
var _ = require('lodash');
var db = require('../../models');
var userFriend = require('./user.friend');
var userFollow = module.exports;

/**
 * @api {post} api/db/user/follow/:id?access_token add follow to the user
 * @apiName AddFollow
 * @apiGroup User_Follow
 * @apiPermission Admin
 *
 * @apiParam {UUID} id New follow id
 * @apiParam {String} access_token
 *
 * @apiError 400 JSON has information about error
 */
userFollow.addFollow = function (req, callback) {
    if (!req.params && !req.params.id) {
        callback({
            err: 'follow id is empty'
        });
    }
    async.waterfall([
        function (cb) {
            db.models.user.find({
                where: {
                    id: req.user.id
                }
            }).complete(cb);
        },
        function (user, cb) {
            db.models.user.find({
                where: {
                    id: req.params.id
                }
            }).complete(function (err, followUser) {
                cb(err, user, followUser);
            });
        },
        function (user, followUser, cb) {
            user.addUserFollow(followUser).complete(function (err) {
                cb(err);
            });
        }
    ], function (err) {
        callback(err, {});
    });
};


/**
 * @api {delete} api/db/user/follow/:id?access_token delete follow from the user
 * @apiName DeleteFollow
 * @apiGroup User_Follow
 * @apiPermission User
 *
 * @apiParam {UUID} id New follow id
 * @apiParam {String} access_token
 *
 * @apiError 400 JSON has information about error
 */
userFollow.deleteFollow = function (req, cb) {
    db.models.userFollow.find({
        where: db.Sequelize.and([
            {
                userId: req.user.id
            }, {
                userFollowId: req.params.id
            }
        ])
    }).complete(function (err, userFollow) {
        userFollow.destroy().complete(function (err) {
            cb(err, {});
        });
    });
};

/**
 * @api {get} api/db/user/friends?access_token=?&eventId=? Show friends of the user
 * @apiName ShowFriends
 * @apiGroup User_Follow
 * @apiPermission UserSelf
 * @apiDescription Returns count and array of users in rows:[]
 *
 * @apiParam {UUID} eventId EventId to show if user is invited (optional)
 * @apiParam {String} query Filter by email and names fields (optional)
 * @apiParam {Number} offset Skip some users (default: 0)
 * @apiParam {Number} limit Limit amount of users (default: 100)
 * @apiParam {String} access_token
 *
 * @apiSuccess {Integer} count count of users (friends)
 * @apiSuccess {User} rows Array of users (friends)
 * @apiSuccess {Boolean} rows/isInvited Is the user invited
 * @apiSuccess {Boolean} rows/isGoing Is the user going to event
 *
 * @apiError 400 JSON has information about error
 */
userFollow.showFriends = function (req, callback) {
    if (!req.params && !req.params.id) {
        return callback({
            err: 'follow id is empty'
        });
    }
    async.waterfall([
        function (cb) {
            userFriend.getFriendsIds(req.user.id, function (err, ids) {
                var searchSubQuery;
                if (req.query.query && req.query.query.length) {
                    searchSubQuery = db.Sequelize.or({
                        email: {
                            $like: '%' + req.query.query +
                            '%'
                        }
                    }, {
                        firstName: {
                            $like: '%' + req.query.query +
                            '%'
                        }
                    }, {
                        middleName: {
                            $like: '%' + req.query.query +
                            '%'
                        }
                    }, {
                        lastName: {
                            $like: '%' + req.query.query +
                            '%'
                        }
                    });
                }
                var whereQuery = db.Sequelize.and({
                        id: {
                            in: ids
                        }
                    },
                    searchSubQuery
                );
                db.models.user.findAndCountAll({
                    where: whereQuery,
                    limit: req.query.limit || 100,
                    offset: req.query.offset || 0,
                }).then(function (users) {
                    cb(null, users);
                }).catch(cb);
            });
        },
        function (users, cb) {
            async.map(users.rows, function (user, cbUser) {
                var userResult = user.toJSON();
                async.parallel({
                    isInvited: function (cbInvited) {
                        db.models.eventInvite.find({
                            where: {
                                eventId: req.query
                                    .eventId,
                                toUserId: user.id
                            }
                        }).then(function (invite) {
                            cbInvited(null, !!
                                invite);
                        }).catch(cbInvited);
                    },
                    isGoing: function (cbGoing) {
                        db.models.userGoing.find({
                            where: {
                                eventId: req.query
                                    .eventId,
                                userId: user.id
                            }
                        }).then(function (going) {
                            cbGoing(null, !!
                                going);
                        }).catch(cbGoing);
                    }
                }, function (err, result) {
                    _.merge(result, userResult);
                    return cbUser(null, result);
                });
            }, function (err, usersResult) {
                users.rows = usersResult;
                cb(!!err, users);
            });
        }
    ], callback);
};

/**
 * @api {get} api/db/user/:userId/friends/ Show friends ids of the user
 * @apiName ShowFriendsIds
 * @apiGroup User_Follow
 * @apiPermission User
 *
 * @apiParam {UUID} userId user to show freiends
 * @apiParam {String} access_token
 *
 * @apiError 400 JSON has information about error
 */
userFollow.showFriendsIds = function (req, callback) {
    if (!req.params && !req.params.id) {
        callback({
            err: 'follow id is empty'
        });
    }
    userFriend.getFriendsIds(req.params.userId, callback);
};
