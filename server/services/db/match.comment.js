'use strict';
var _ = require('lodash');
var db = require('../../models');
var async = require('async');
var errorHandler = require('../../common/error-handler');

var matchComment = module.exports;

/**
 * @api {get} api/db/want/match/comment/:id Get comments of match
 * @apiDescription Result is {count: 2, rows: []}
 * @apiName MatchGetComments
 * @apiGroup Match_Comment
 * @apiPermission User
 *
 * @apiParam {UUID} id matchId to comment
 * @apiParam {Number} offset Skip some comments (default: 0)
 * @apiParam {Number} limit Limit amount of comments (default: 100)
 *
 * @apiUse MatchCommentSuccess
 *
 * @apiError 400 JSON has information about error
 */
matchComment.getComment = function(req, callback) {
    if (!req.params && !req.params.id) {
        callback(errorHandler.errors.needEventId);
    }
    async.waterfall([

        function(cb) {
            db.models.match.find({
                where: {
                    id: req.params.id
                }
            }).complete(function(err, match) {
                if (!match) {
                    return cb(errorHandler.errors.eventNotFound());
                }
                cb(null);
            });
        },
        function(cb) {
            db.models.matchComment.findAndCountAll({
                where: {
                    matchId: req.params.id
                },
                limit: req.query.limit || 100,
                offset: req.query.offset || 0,
                include: [{
                    model: db.models.user,
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }, {
                    model: db.models.user,
                    as: 'replyToUser',
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }, {
                    model: db.models.user,
                    as: 'mentionUser',
                    attributes: [
                        'firstName',
                        'middleName',
                        'lastName',
                        'avatar',
                        'sex'
                    ]
                }],
                order: ['createdAt']
            }).complete(cb);
        },
    ], function(err, comments) {
        callback(err, comments);
    });
};
/**
 * @api {post} api/db/want/match/comment/:id Add comment to match
 * @apiName MatchAddComment
 * @apiGroup Match_Comment
 * @apiPermission User
 *
 * @apiParam {UUID} id matchId to comment
 * @apiUse MatchCommentParams
 *
 * @apiUse MatchCommentSuccess
 *
 * @apiError 400 JSON has information about error
 */
matchComment.addComment = function(req, callback) {
    var userId = req.query.userId || req.user.id;
    async.waterfall([

        function(cb) {
            db.models.match.find({
                where: {
                    id: req.params.id
                }
            }).then(function(event) {
                cb(null, event);
            }).catch(cb);
        },
        function(match, cb) {
            db.models.user.find({
                where: {
                    id: userId
                }
            }).then(function(user) {
                cb(null, user, match);
            }).catch(cb);
        },
        function(user, match, cb) {
            var commentBody = _.merge({
                userId: userId,
                matchId: match.id,
                replyTo: req.body.replyTo || null
            }, req.body);
            db.models.matchComment.create(commentBody)
                .then(function(comment) {
                    cb(null, comment);
                })
                .catch(cb);
        }
    ], function(err, comment) {
        callback(err, comment);
    });
};
/**
 * @api {delete} api/db/want/match/comment/:id Remove comment from match
 * @apiName MatchRemoveComment
 * @apiGroup Match_Comment
 * @apiPermission Admin
 *
 * @apiParam {UUID} id commentId to remove comment
 *
 * @apiError 400 JSON has information about error
 */
matchComment.removeComment = function(req, callback) {
    async.waterfall([

        function(cb) {
            db.models.matchComment.destroy({
                where: {
                    id: req.params.id
                }
            }).complete(function(err) {
                cb(err);
            });
        }
    ], function(err) {
        callback(err);
    });
};
