'use strict';

var request = module.exports;
var async = require('async');
var _ = require('lodash');
var db = require('../../models');

/**
 * @api {get} api/db/user/requests Get requests (invites to me)
 * @apiName GetRequests
 * @apiGroup Requests
 * @apiPermission UserSelf
 * @apiDescription Returns rows of invites to the user
 *
 * @apiParam {Number} offset Skip some activity (default: 0)
 * @apiParam {Number} limit Limit amount of activity (default: 100)
 *
 * @apiSuccess {Guid} id id of the invite
 * @apiSuccess {Enum} type type of the feed: newFollow, newEvent, newLike
 * @apiSuccess {Guid} eventId
 * @apiSuccess {Object} event All info about the event
 * @apiSuccess {Guid} userId id of user to invite from
 * @apiSuccess {Object} user all info about fromUser
 * @apiSuccess {DateTime} createdAt
 * @apiSuccess {DateTime} updatedAt
 *
 * @apiError 400 JSON has information about error
 */
request.getRequests = function(req, callback) {
    async.waterfall([
        function(cb) {
            db.models.eventInvite.findAndCount({
                where: {
                    toUserId: '888' || req.user.id
                },
                limit: req.query.limit || 100,
                offset: req.query.offset || 0,
                order: [
                    ['updatedAt', 'DESC']
                ]
            }).then(function(invites) {
                cb(null, invites);
            }).catch(cb);
        },
        function(invites, cb) {
            async.map(invites.rows, function(invite, cbMap) {
                async.parallel({
                    event: function(cbEvent) {
                        db.models.event.find({
                            where: {
                                id: invite.eventId
                            }
                        }).then(
                            function(event) {
                                cbEvent(null, event);
                            }).catch(cb);
                    },
                    fromUser: function(cbEvent) {
                        db.models.user.find({
                            where: {
                                id: invite.userId
                            }
                        }).then(
                            function(user) {
                                cbEvent(null, user);
                            }).catch(cb);
                    }
                }, function(err, result) {
                    cbMap(null, _.merge(invite.toJSON(),
                        result));
                });
            }, function(err, results) {
                invites.rows = results;
                cb(null, invites);
            });
        },
    ], function(err, results) {
        callback(err, results);
    });
};
