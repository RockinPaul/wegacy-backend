'use strict';
var _ = require('lodash');
var db = require('../../models');
var async = require('async');
var errorHandler = require('../../common/error-handler');
var tagFromString = require('../../common/tag-from-string.js');

var eventPhoto = module.exports;

/**
 * @api {get} api/db/event/photo/:id Get Photos of event
 * @apiDescription Result is {count: 2, rows: []}
 * @apiName EventGetPhotos
 * @apiGroup Event_Photo
 * @apiPermission User
 *
 * @apiParam {UUID} id eventId to photo
 * @apiParam {Number} offset Skip some Photos (default: 0)
 * @apiParam {Number} limit Limit amount of Photos (default: 100)
 *
 * @apiUse PhotoSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventPhoto.getPhoto = function(req, callback) {
    if (!req.params && !req.params.id) {
        callback(errorHandler.errors.needEventId);
    }
    async.waterfall([
        function(cb) {
            db.models.event.find({
                where: {
                    id: req.params.id
                }
            }).complete(function(err, event) {
                if (!event) {
                    return cb(errorHandler.errors.eventNotFound());
                }
                cb(null, event);
            });
        },
        function(event, cb) {
            db.models.photo.findAndCountAll({
                where: {
                    hashTag: {
                        in: tagFromString(event.hashTag)
                    }
                },
                limit: req.query.limit || 100,
                offset: req.query.offset || 0,
                order: [
                    ['externalCreatedTime', 'DESC']
                ]
            }).complete(cb);
        },
    ], function(err, Photos) {
        callback(err, Photos);
    });
};
/**
 * @api {post} api/db/event/photo/:id Add photo to event
 * @apiName EventAddPhoto
 * @apiGroup Event_Photo
 * @apiPermission User
 *
 * @apiParam {UUID} id eventId to photo
 * @apiUse PhotoParams
 *
 * @apiUse PhotoSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventPhoto.addPhoto = function(req, callback) {
    async.waterfall([
        function(cb) {
            db.models.event.find({
                where: {
                    id: req.params.id
                }
            }).complete(cb);
        },
        function(event, cb) {
            db.models.user.find({
                where: {
                    id: req.user.id
                }
            }).complete(function(err, user) {
                cb(err, user, event);
            });
        },
        function(user, event, cb) {
            var PhotoBody = _.merge({
                userId: user.id,
                eventId: event.id,
                replyTo: req.body.replyTo
            }, req.body);
            db.models.photo.create(PhotoBody)
                .complete(function(err, photo) {
                    cb(err, photo);
                });
        }
    ], function(err, photo) {
        callback(err, photo);
    });
};
/**
 * @api {delete} api/db/event/photo/:id Remove photo from event
 * @apiName EventRemovePhoto
 * @apiGroup Event_Photo
 * @apiPermission Admin
 *
 * @apiParam {UUID} id PhotoId to remove photo
 *
 * @apiUse EventSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventPhoto.removePhoto = function(req, callback) {
    async.waterfall([
        function(cb) {
            db.models.photo.destroy({
                where: {
                    id: req.params.id
                }
            }).complete(function(err) {
                cb(err);
            });
        }
    ], function(err) {
        callback(err);
    });
};
