'use strict';

var async = require('async');
var db = require('../../models');
var config = require('../../config');
var rpc = require('../../common/amqp/amqp-rpc');

var match = module.exports;

/**
 * @api {put} api/db/want?userId=&access_token=? startSearching for want to go
 * @apiName wantToGoAddMatch
 * @apiGroup WantToGo
 * @apiPermission User
 *
 * @apiParam {UUID} userId (optional) userId
 * @apiParam {Number} lat latitude of current user location
 * @apiParam {Number} lon longitude of current user location
 *
 * @apiSuccess {Number} count users wantToGo in the city
 *
 * @apiError 400 JSON has information about error
 */
match.setWantToGo = function(req, callback) {
    var userId = req.query.userId || req.user.id;
    db.models.user.update({
        isWantToGo: true,
        lat: req.body.lat,
        lon: req.body.lon
    }, {
        where: {
            id: userId
        }
    }).then(function(updatedUser) {
        rpc.call('get/db/want/search', req, function(err) {
            callback(err, updatedUser);
        });
    });
};

var updateMatchStatus = function(matchId, cb) {
    async.waterfall([

            function(cb) {
                db.models.match.find({
                    where: {
                        id: matchId
                    },
                    include: [{
                        model: db.models.user,
                        attributes: ['id']
                    }, {
                        model: db.models.event,
                        include: [{
                            model: db.models.user,
                            as: 'userGoing',
                            attributes: ['id', 'avatar']
                        }]
                    }]
                }).then(function(match) {
                    cb(null, match.toJSON());
                }).catch(cb);
            },
            function(match, cb) {
                rpc.call('get/db/event/:id', {
                    params: {
                        id: match.eventId
                    },
                    user: {
                        id: config.security.adminUser.id
                    }
                }, function(err, event) {
                    var usersGoing = match.event.userGoing;
                    match.event = event;
                    cb(null, usersGoing, match);
                });
            },
            function(usersGoing, match, cb) {
                var goingUsersIds = usersGoing.map(function(user) {
                    return user.id;
                });
                match.users.map(function(user) {
                    user.isGoing = (goingUsersIds.indexOf(user.id) !== -1);
                });
                cb(null, match);
            },
            function(match, cb) {
                var notGoingUsers = match.users.filter(function(user) {
                    return user.isGoing === false;
                });
                cb(null, match, notGoingUsers);
            },
            function(match, notGoingUsers, cb) {
                var status;
                if (notGoingUsers.length === 0 &&
                    (match.users.length === config.want.companyCnt)) {
                    status = 'allUsersJoined';
                } else if (match.users.length === config.want.companyCnt) {
                    status = 'needJoinAllUsers';
                } else {
                    status = 'searchAgain';
                }
                if (match.status === status) {
                    return cb(null, match);
                }
                console.info('new status', status);
                db.models.match.update({
                    status: status
                }, {
                    where: {
                        id: match.id
                    }
                }).then(function() {
                    cb(null, match);
                }).catch(cb);
            }
        ],
        function(err, match) {
            cb(err, match);
        });
};

var matchUsersToEvent = function(companyUsers, eventId, cb) {
    async.waterfall([

        function(cb) {
            db.models.match.create({
                eventId: eventId,
                status: 'needJoinAllUsers'
            }).then(function(matchDb) {
                cb(null, matchDb);
            }).catch(cb);
        },
        function(matchDb, cb) {
            async.each(companyUsers, function(user, cb) {
                db.models.user.update({
                    matchId: matchDb.id
                }, {
                    where: {
                        id: user.user.id
                    }
                }).then(function(userDb) {
                    cb(null, userDb);
                }).catch(cb);
            }, function(err) {
                cb(err, matchDb);
            });
        },
        function(matchDb, cb) {
            updateMatchStatus(matchDb, function(err, match) {
                cb(err, match);
            });
        }
    ], function(err, matchDb) {
        cb(err, matchDb);
    });
};

var selectOneCompanyUsers = function(usersResult, cb) {
    async.waterfall([

        function(cb) {
            cb(null, usersResult);
        },
        function(usersResult, cb) {
            var uniqueEvents = {};
            usersResult.map(function(user) {
                user.matchedEvents.map(function(event) {
                    if (!uniqueEvents[event.id]) {
                        uniqueEvents[event.id] = {
                            event: event,
                            users: []
                        };
                    }
                    uniqueEvents[event.id].users.push(user);
                });
            });
            cb(null, uniqueEvents);
        },
        function(uniqueEvents, cb) {
            var uniqueEventsWithCompanyCount =
                Object.keys(uniqueEvents).filter(function(eventId) {
                    return uniqueEvents[eventId].users.length >= config.want.companyCnt;
                });
            cb(null, uniqueEvents, uniqueEventsWithCompanyCount);
        },
        function(uniqueEvents, uniqueEventsWithCompanyCount, cb) {
            if (!uniqueEventsWithCompanyCount.length) {
                return cb(null, null);
            }
            var index = Math.floor(Math.random() * uniqueEventsWithCompanyCount.length);
            var firstEvent = uniqueEvents[uniqueEventsWithCompanyCount[index]];
            cb(null, firstEvent);
        },
        function(firstEvent, cb) {
            if (!firstEvent) {
                return cb(null, []);
            }
            var companyUsers = firstEvent.users.slice(0, config.want.companyCnt);
            var companyUsersIds = companyUsers.map(function(user) {
                return user.user.id;
            });
            var newUsersResult = usersResult.filter(function(user) {
                return companyUsersIds.indexOf(user.user.id) === -1;
            });
            matchUsersToEvent(companyUsers, firstEvent.event.id, function() {
                cb(null, newUsersResult);
            });
        }
    ], function(err, newUsersResult) {
        cb(err, newUsersResult);
    });
};

var selectOneCompanyUsersRecursive = function(usersResult, cb) {
    selectOneCompanyUsers(usersResult, function(err, newUsersResult) {
        if (newUsersResult.length < config.want.companyCnt) {
            return cb(null, newUsersResult);
        }
        selectOneCompanyUsersRecursive(newUsersResult, cb);
    });
};

/**
 * @api {get} api/db/want/search?access_token=? startSearching for all users
 * @apiName wantToGoGetMatchSearching
 * @apiGroup WantToGo
 * @apiPermission User
 *
 * @apiParam {UUID} userId (optional) userId
 *
 * @apiError 400 JSON has information about error
 */
var searching = false;
var needRestartSearchLater = false;
match.search = function(req, callback) {
    if (searching) {
        needRestartSearchLater = true;
        return callback(null, {
            status: 'already searching'
        });
    }
    searching = true;
    var usersResult = [];
    async.waterfall([

        function(cb) {
            db.models.user.findAll({
                where: {
                    matchId: null,
                    isWantToGo: true
                }
            }).then(function(users) {
                cb(null, users);
            }).catch(cb);
        },
        function(users, cb) {
            async.eachLimit(users, 1, function(user, cb) {
                rpc.call('get/search/event/feed/near', {
                    query: {
                        lat: user.lat,
                        lon: user.lon,
                        distance: '3000km'
                    },
                    user: {
                        id: user.id
                    }
                }, function(err, events) {
                    usersResult.push({
                        user: user.toJSON(),
                        matchedEvents: events.hits.hits
                    });
                    cb(null, users);
                });
            }, function(err) {
                cb(err, usersResult);
            });
        },
        //complectate uncomplete matches in status 'searchAgain'
        function(usersResult, cb) {
            db.models.match.findAll({
                where: {
                    status: 'searchAgain'
                },
                include: [{
                    model: db.models.event,
                    attributes: ['id']
                }, {
                    model: db.models.user,
                    attributes: ['id']
                }]
            }).then(function(unfullMatches) {
                var addUsersToMatch = {};
                var unfullMatchesIds = unfullMatches.map(function(match) {
                    return match.id;
                });
                db.models.matchRefuse.findAll({
                    where: {
                        matchId: {
                            $in: unfullMatchesIds
                        }
                    }
                }).then(function(matchesRefused) {
                    unfullMatches.map(function(unfullMatch) {
                        usersResult.map(function(user) {
                            user.matchedEvents.map(function(event) {
                                var userRefused = false;
                                matchesRefused.map(function(matchRefuse) {
                                    if (matchRefuse.userId === user.user.id &&
                                        matchRefuse.matchId === unfullMatch.id) {
                                        userRefused = true;
                                    }
                                });
                                if (event.id === unfullMatch.event.id &&
                                    unfullMatch.users.length < config.want.companyCnt &&
                                    !userRefused) {
                                    addUsersToMatch[user.user.id] = unfullMatch.id;
                                    unfullMatch.users.push({
                                        id: user.user.id
                                    });
                                }
                            });
                        });
                    });
                    async.forEachOf(addUsersToMatch, function(userToMatchId, userId, cb) {
                        db.models.user.update({
                            matchId: userToMatchId
                        }, {
                            where: {
                                id: userId
                            }
                        }).then(function() {
                            updateMatchStatus(userToMatchId, function(err) {
                                cb(err);
                            });
                        }).catch(cb);
                        console.info(userId, ' = matchId = ', userToMatchId);
                    }, function(err) {
                        cb(err, usersResult);
                    });

                }).catch(cb);

            }).catch(cb);
        },
        function(usersResult, cb) {
            selectOneCompanyUsersRecursive(usersResult, cb);
        }
    ], function(err) {
        searching = false;
        if (needRestartSearchLater) {
            needRestartSearchLater = false;
            match.search(req, function() {});
        }
        callback(err, {});
    });
};


var findCurrentMatch = function(theUser) {
    return function(cb) {
        if (theUser.isWantToGo && !theUser.matchId) {
            return cb(null, {
                status: 'searching'
            });
        }
        if (!theUser.isWantToGo) {
            return cb(null, {
                status: 'readyToStartSearch'
            });
        }
        async.waterfall([

            function(cb) {
                db.models.match.find({
                    where: {
                        id: theUser.matchId
                    },
                    include: [{
                        model: db.models.user
                    }, {
                        model: db.models.event,
                        include: [{
                            model: db.models.user,
                            as: 'userGoing',
                            attributes: ['id', 'avatar']
                        }]
                    }]
                }).then(function(match) {
                    cb(null, match.toJSON());
                }).catch(cb);
            },
            function(match, cb) {
                rpc.call('get/db/event/:id', {
                    params: {
                        id: match.eventId
                    },
                    user: {
                        id: theUser.id
                    }
                }, function(err, event) {
                    var usersGoing = match.event.userGoing;
                    match.event = event;
                    cb(null, usersGoing, match);
                });
            },
            function(usersGoing, match, cb) {
                var goingUsersIds = usersGoing.map(function(user) {
                    return user.id;
                });
                match.users.map(function(user) {
                    user.isGoing = (goingUsersIds.indexOf(user.id) !== -1);
                });
                cb(null, match);
            },
            function(match, cb) {
                var notGoingUsers = match.users.filter(function(user) {
                    return user.isGoing === false;
                });
                cb(null, match, notGoingUsers);
            },
            function(match, notGoingUsers, cb) {
                cb(null, match);
            }
        ], function(err, match) {
            cb(null, match);
        });
    };
};


/**
 * @api {get} api/db/want?userId=&access_token=? startSearching for want to go
 * @apiName wantToGoGetMatch
 * @apiGroup WantToGo
 * @apiPermission User
 *
 * @apiParam {UUID} userId (optional) userId
 *
 * @apiError 400 JSON has information about error
 */
match.getWantToGo = function(req, callback) {
    var userId = req.query.userId || req.user.id;
    var users = {};
    async.waterfall([

        function(cb) {
            db.models.user.find({
                where: {
                    id: userId
                }
            }).then(function(theUser) {
                cb(null, theUser);
            }).catch(cb);
        },
        function(theUser, cb) {
            async.series({
                peopleWantToGoWithYou: function(cb) {
                    db.models.user.findAll({
                        where: {
                            isWantToGo: true,
                            id: {
                                $not: userId
                            },
                            city: theUser.city,
                            matchId: null
                        }
                    }).then(function(usersResult) {
                        users = usersResult;
                        cb(null, users.length);
                    }).catch(callback);
                },
                avatars: function(cb) {
                    var avatars = users.filter(function(user) {
                        return user.avatar !== null;
                    }).map(function(user) {
                        return user.avatar;
                    });
                    return cb(null, avatars);
                },
                match: findCurrentMatch(theUser)

            }, function(err, results) {
                cb(err, results);
            });
        }
    ], function(err, results) {
        callback(err, results);
    });

};

/**
 * @api {put} api/db/want/join?userId=&access_token=? join to current match
 * event
 * @apiName joinToCurrentMatch
 * @apiGroup WantToGo
 * @apiPermission User
 *
 * @apiParam {UUID} userId (optional) userId
 *
 * @apiError 400 JSON has information about error
 */
match.join = function(req, callback) {
    var userId = req.query.userId || req.user.id;
    async.waterfall([

        function(cb) {
            db.models.user.find({
                where: {
                    id: userId,
                    matchId: {
                        $not: null
                    }
                }
            }).then(function(user) {
                cb(null, user);
            }).catch(cb);
        },
        function(user, cb) {
            if (!user) {
                return cb('user not found');
            }
            db.models.match.find({
                where: {
                    id: user.matchId
                }
            }).then(function(match) {
                cb(null, user, match);
            }).catch(cb);
        },
        function(user, match, cb) {
            if (!match) {
                return cb('match not found');
            }
            db.models.event.find({
                where: {
                    id: match.eventId
                }
            }).then(function(event) {
                cb(null, user, match, event);
            }).catch(cb);
        },
        function(user, match, event, cb) {
            user.addGoing(event).complete(function() {
                event.sendToSearch(function() {
                    cb(null, match);
                });
            });
        },
        function(match, cb) {
            updateMatchStatus(match.id, function(err) {
                cb(err, match);
            });
        },
    ], function(err, match) {
        callback(err, match);
    });
};

/**
 * @api {put} api/db/want/refuse?userId=&access_token=? refuse from current match
 * event
 * @apiName refuseFromCurrentMatch
 * @apiGroup WantToGo
 * @apiPermission User
 *
 * @apiParam {UUID} userId (optional) userId
 *
 * @apiError 400 JSON has information about error
 */
match.refuse = function(req, callback) {
    var userId = req.query.userId || req.user.id;
    async.waterfall([

        function(cb) {
            db.models.user.find({
                where: {
                    id: userId,
                    matchId: {
                        $not: null
                    }
                }
            }).then(function(user) {
                cb(null, user);
            }).catch(cb);
        },
        function(user, cb) {
            if (!user) {
                return cb('user not found');
            }
            db.models.match.find({
                where: {
                    id: user.matchId
                }
            }).then(function(match) {
                cb(null, user, match);
            }).catch(cb);
        },
        function(user, match, cb) {
            db.models.matchRefuse.create({
                userId: user.id,
                matchId: match.id
            }).then(function() {
                cb(null, user, match);
            }).catch(cb);
        },
        function(user, match, cb) {
            user.updateAttributes({
                isWantToGo: false,
                matchId: null
            }).then(function(userDb) {
                cb(null, userDb, match);
            }).catch(cb);
        },
        function(user, match, cb) {
            updateMatchStatus(match.id, function(err, match) {
                cb(err, match);
            });
        }
    ], function(err) {
        callback(err, {});
    });
};
