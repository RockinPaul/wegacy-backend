'use strict';
var db = require('../../models');
var async = require('async');

var eventLike = module.exports;

/**
 * @api {post} api/db/event/like/:id Add like to event
 * @apiName EventAddLike
 * @apiGroup Event_Like
 * @apiPermission User
 *
 * @apiParam {UUID} id eventId to like
 *
 * @apiUse EventSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventLike.addLike = function (req, callback) {
    async.waterfall([
        function (cb) {
            db.models.event.find({
                where: {
                    id: req.params.id
                }
            }).complete(cb);
        },
        function (event, cb) {
            db.models.user.find({
                where: {
                    id: req.user.id
                }
            }).complete(function (err, user) {
                cb(err, event, user);
            });
        },
        function (event, user, cb) {
            event.addUserLike(user).complete(function(err, addLikeResult){
                cb(err, event, addLikeResult);
            });
        },
        function(event, addLikeResult, cb){
            if(!addLikeResult){
                return cb(null, event); //dont need increment, the same user
            }
            event.increment('likeCount').complete(function(err, event){
                cb(err, event);
            });
        }
    ], function (err, event) {
        callback(err, event);
    });
};
/**
 * @api {delete} api/db/event/like/:id Remove like from event
 * @apiName EventRemoveLike
 * @apiGroup Event_Like
 * @apiPermission User
 *
 * @apiParam {UUID} id eventId to unlike
 *
 * @apiUse EventSuccess
 *
 * @apiError 400 JSON has information about error
 */
eventLike.removeLike = function (req, callback) {
    async.waterfall([
        function (cb) {
            db.models.userLike.find({
                where: {
                    eventId: req.params.id,
                    userId: req.user.id
                }
            }).complete(cb);
        },
        function(userLike, cb){
            db.models.event.find({
                where: {
                    id: req.params.id
                }
            }).complete(function(err, event){
                cb(err, userLike, event);
            });
        },
        function(userLike, event, cb){
            userLike.destroy().complete(function(err, result){
                cb(err, result, event);
            });
        },
        function(result, event, cb){
            if(result && result.length && result[0].affectedRows){
                event.decrement('likeCount').complete(cb);
            } else {
                cb('like not found');
            }
        }
    ], function (err, event) {
        callback(err, event);
    });
};
