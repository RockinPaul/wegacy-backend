'use strict';
var rpc = require('../../common/amqp/amqp-rpc');
var user = require('./user.js');
var eventSrv = require('./event-srv.js');
var search = require('./search.js');
var userCategory = require('./user.category.js');
var userGoing = require('./user.going.js');
var userFollow = require('./user.follow.js');
var eventLike = require('./event.like.js');
var eventComment = require('./event.comment.js');
var eventPhoto = require('./event.photo.js');
var eventInvite = require('./event.invite.js');
var abuse = require('./abuse.js');
var errorSrv = require('./error.js');
var feed = require('./feed.js');
var match = require('./match.js');
var matchComment = require('./match.comment.js');
var instagram = require('./instagram.js');
var request = require('./request.js');

function DbService() {
    rpc.on('get/db/test', user.test);

    rpc.on('get/db/user/requests', request.getRequests);

    rpc.on('get/db/user/friends', userFollow.showFriends);
    rpc.on('get/db/user/:userId/friends', userFollow.showFriendsIds);

    rpc.on('put/db/want', match.setWantToGo);
    rpc.on('put/db/want/join', match.join);
    rpc.on('put/db/want/refuse', match.refuse);
    rpc.on('get/db/want', match.getWantToGo);
    rpc.on('get/db/want/search', match.search);

    rpc.on('get/db/want/match/comment/:id', matchComment.getComment);
    rpc.on('post/db/want/match/comment/:id', matchComment.addComment);
    rpc.on('delete/db/want/match/comment/:id', matchComment.removeComment);


    rpc.on('get/db/users', user.getAllUsers);
    rpc.on('get/db/user/:id', user.getUserById);
    rpc.on('get/db/user', user.getUserSelfByToken);
    rpc.on('put/db/user/password', user.updateUserPassword);
    rpc.on('put/db/user/:id', user.updateUserById);
    rpc.on('put/db/user', user.updateUserSelfByToken);
    rpc.on('delete/db/user', user.deleteUserById);
    rpc.on('get/db/users/search', user.searchUsersByParams);
    rpc.on('post/db/user', user.createNewUser);
    rpc.on('post/db/user/social/:socialType', user.createBySocialNetwork);
    rpc.on('post/db/user/abuse/:id', abuse.create);
    rpc.on('post/db/user/error/:id', errorSrv.create);
    rpc.on('get/db/user/error', errorSrv.showAll);

    rpc.on('post/db/user/follow/:id', userFollow.addFollow);
    rpc.on('delete/db/user/follow/:id', userFollow.deleteFollow);

    rpc.on('get/db/user/follow', user.getFollow);
    rpc.on('get/db/user/following', user.getFollowing);

    rpc.on('get/db/user/categories/list', userCategory.getUserCategoriesList);
    rpc.on('get/db/user/categories', userCategory.getUserCategories);
    rpc.on('put/db/user/categories', userCategory.putUserCategories);

    rpc.on('get/db/user/going', userGoing.getUserGoing);
    rpc.on('get/db/user/invites', user.getUserInvites);
    rpc.on('put/db/user/going/:eventId', userGoing.addUserGoing);
    rpc.on('delete/db/user/going/:eventId', userGoing.deleteUserGoing);

    rpc.on('get/db/events', eventSrv.getAllEvents);
    rpc.on('get/db/events/visited', eventSrv.getVisitedEvents);
    rpc.on('get/db/event/:id', eventSrv.getEventById);
    rpc.on('delete/db/event/:eventId', eventSrv.deleteEventById);
    rpc.on('post/db/event', eventSrv.createNewEvent);
    rpc.on('put/db/event/:id', eventSrv.updateEventById);
    rpc.on('get/db/events/search', eventSrv.searchEventsByParams);

    rpc.on('get/db/event/:eventId/going', user.getGoingUsersByEvent);

    rpc.on('post/event/:eventId/invite', eventInvite.addInviteToAllFriends);
    rpc.on('post/event/:eventId/invite/:userId', eventInvite.addInvite);

    rpc.on('post/db/reindex', search.reindex);

    rpc.on('post/db/event/like/:id', eventLike.addLike);
    rpc.on('delete/db/event/like/:id', eventLike.removeLike);

    rpc.on('get/db/event/comment/:id', eventComment.getComment);
    rpc.on('post/db/event/comment/:id', eventComment.addComment);
    rpc.on('delete/db/event/comment/:id', eventComment.removeComment);

    rpc.on('get/db/event/photo/:id', eventPhoto.getPhoto);
    rpc.on('post/db/event/photo/:id', eventPhoto.addPhoto);
    rpc.on('delete/db/event/photo/:id', eventPhoto.removePhoto);

    rpc.on('put/feed/newEvent', feed.newEvent);
    rpc.on('put/feed/newLike', feed.newLike);
    rpc.on('put/feed/newInvite', feed.newInvite);
    rpc.on('put/feed/newFollow', feed.newFollow);
    rpc.on('put/feed/newGoing', feed.newGoing);
    rpc.on('put/feed/newComment', feed.newComment);

    rpc.on('get/feed', feed.getFeed);

    rpc.on('get/db/instagram', instagram.getInstagram);
    rpc.on('post/db/instagram', instagram.postInstagram);
    rpc.on('post/db/instagram/subscribe/:eventId', instagram.subscribe);
    rpc.on('post/db/instagram/refresh/:eventId', instagram.refreshPhotos);
    rpc.on('get/db/instagram/subscriptions', instagram.getSubscriptions);
    rpc.on('get/db/instagram/subscriptions/unsubscribe', instagram.unsubscribeAll);
}

module.exports = DbService;
