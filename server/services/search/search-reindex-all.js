'use strict';

var client = require('./elasticsearch-client');
var async = require('async');

var reindex = {};

var indexes = ['users', 'events'];

reindex.recreateIndex = function(callback) {
    async.series([
        function(cb) {
            async.each(indexes, function(indexName, cbIndex) {
                client.indices.exists({
                    index: indexName
                }, function(err, exist) {
                    if (exist) {
                        client.indices.delete({
                            index: indexName
                        }, function(err) {
                            cbIndex(err);
                        });
                    } else {
                        cbIndex();
                    }
                });
            }, function(err) {
                cb(err);
            });
        },
        function(cb) {
            client.indices.create({
                index: 'events',
                body: {
                    settings: {
                        'analysis': {
                            'analyzer': {
                                'ru': {
                                    'type': 'custom',
                                    'tokenizer': 'standard',
                                    "filter": ['lowercase',
                                        'my_ngram',
                                        'ru_stemming',
                                        'russian_morphology',
                                        'english_morphology',
                                        'ru_stopwords',
                                    ],
                                },
                            },
                            'filter': {
                                'ru_stopwords': {
                                    'type': 'stop',
                                    'stopwords': 'а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я,a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with',
                                },
                                'ru_stemming': {
                                    'type': 'snowball',
                                    'language': 'Russian',
                                },
                                'my_ngram': {
                                    type: 'edgeNGram',
                                    min_gram: 3,
                                    max_gram: 20,
                                    token_chars: ['letter', 'digit']
                                }
                            },

                        }
                    }
                }
            }, function(err) {
                cb(err);
            });
        },
        function(cb) {
            cb(null);
        },
        function(cb) {
            client.indices.putMapping({
                index: 'events',
                type: 'event',
                body: {
                    event: {
                        properties: {
                            location: {
                                type: 'geo_point'
                            },
                            title: {
                                type: 'string',
                                analyzer: 'ru'
                            },
                            address: {
                                type: 'string',
                                analyzer: 'ru'
                            },
                            eventDetails: {
                                type: 'string',
                                analyzer: 'ru'
                            }
                        }
                    }
                }
            }, function(err) {
                cb(err);
            });
        }
    ], function(err) {
        callback(err);
    });
};

reindex.reindexAll = function(req, cb) {
    reindex.recreateIndex(function() {
        var rpc = require('../../common/amqp/amqp-rpc');
        rpc.call('post/db/reindex', {
            body: {}
        }, function(result) {
            cb(null, result);
        });
    });
};

module.exports = reindex;
