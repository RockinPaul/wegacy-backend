'use strict';

var logger = require('../../config/logger');
var client = require('./elasticsearch-client');

var searchUser = {};

searchUser.saveToElastic = function (req, cb) {
    client.index({
        index: 'users',
        id: req.body.id,
        type: 'user',
        body: req.body
    }, function(err, res){
        logger.trace('save user to elastic search', req.body.id, req.body.lastName);
        cb(err, res);
    });
};
searchUser.deleteFromElastic = function (req, cb) {
    client.delete({
        index: 'users',
        id: req.body.id,
        type: 'user'
    }, function(){
        logger.trace('delete user from elastic search', req.body.id);
        cb(null, {answer: 1});
    });
};

module.exports = searchUser;
