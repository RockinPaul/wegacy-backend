'use strict';

var rpc = require('../../common/amqp/amqp-rpc');
var searchUser = require('./search-user.js');
var searchEvent = require('./search-event.js');
var reindex = require('./search-reindex-all.js');

function DbService() {
    rpc.on('post/search/user', searchUser.saveToElastic);
    rpc.on('delete/search/user', searchUser.deleteFromElastic);

    rpc.on('get/search/event/test', searchEvent.test);
    rpc.on('post/search/event', searchEvent.saveToElastic);
    rpc.on('delete/search/event', searchEvent.deleteFromElastic);

    rpc.on('get/search/event/feed/all', searchEvent.feed('all'));
    rpc.on(
        'get/search/event/feed/now', searchEvent.feed('now'));
    rpc.on(
        'get/search/event/feed/near', searchEvent.feed('near'));
    rpc.on(
        'get/search/event/feed/friends', searchEvent.feed('friends'));

    rpc.on('get/search/reindex', reindex.reindexAll);
}

module.exports = DbService;
