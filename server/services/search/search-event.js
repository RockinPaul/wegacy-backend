'use strict';

var logger = require('../../config/logger');
var client = require('./elasticsearch-client');
var rpc = require('../../common/amqp/amqp-rpc');
var async = require('async');
var _ = require('lodash');

var searchEvent = {};

function getUserCategories(userId, cb) {
    rpc.call('get/db/user/categories', {
        user: {
            id: userId
        }
    }, function (err, categories) {
        cb(err, categories.map(function (category) {
            return category.id;
        }));
    });
}

function getUserFriends(userId, cb) {
    rpc.call('get/db/user/:userId/friends', {
        params: {
            userId: userId
        }
    }, function (err, friendsIds) {
        cb(err, friendsIds);
    });
}

function getEvent(user, eventId, cb) {
    rpc.call('get/db/event/:id', {
        params: {
            id: eventId
        },
        user: user
    }, function (err, event) {
        cb(err, event);
    });
}

function getInvites(user, userId, cb) {
    rpc.call('get/db/user/invites', {
        params: {
            id: userId
        },
        user: user
    }, function (err, invites) {
        cb(err, invites);
    });
}

function getUser(user, userId, cb) {
    rpc.call('get/db/user/:id', {
        params: {
            id: userId
        },
        user: user
    }, function (err, user) {
        rpc.call('get/db/user/invites', {
            params: {
                id: userId
            },
            user: user
        }, function (err, invites) {
            user.invites = invites;
            cb(err, user);
        });
    });
}

var privacyQuery = function (user, friendsIds) {
    var invitedEventIds = user.invites.map(function (invite) {
        return invite.eventId;
    });
    return {
        or: [
            {
                query: {
                    match: {
                        privacy: 'Public'
                    }
                }
            },
            {
                query: {
                    match: {
                        privacy: 'OpenInvite'
                    }
                }
            },
            {
                and: [
                    {
                        query: {
                            match: {
                                privacy: 'InviteOnly'
                            }
                        }
                    },
                    {
                        query: {
                            terms: {
                                _id: invitedEventIds
                            }
                        }
                    }
                ]
            },
            {
                and: [
                    {
                        query: {
                            match: {
                                privacy: 'InvitedGuestsAndFriends'
                            }
                        }
                    },
                    {
                        or: [
                            {
                                query: {
                                    terms: {
                                        _id: invitedEventIds
                                    }
                                }
                            },
                            {
                                query: {
                                    terms: {
                                        userId: friendsIds
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
};

function getAllQuery(categories, friendsIds, currentLocation, title, distance, user) {
    return {
        'function_score': {
            query: {
                filtered: {
                    query: {
                        bool: {
                            must: [
                                {
                                    terms: {
                                        categoryId: categories
                                    }
                                }, {
                                    match: {
                                        city: user.city
                                    }
                                }, {
                                    range: {
                                        endDateTime: {
                                            gte: "now"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    filter: {
                        bool: {
                            must: [
                                privacyQuery(user, friendsIds)
                            ]
                        }
                    },
                }
            },
            functions: [
                {
                    exp: {
                        startDateTime: {
                            origin: "now",
                            offset: '0',
                            decay: 0.3,
                            scale: '10d'
                        }
                    }
                }
            ],
            score_mode: 'multiply'
        }
    };
}

function getNowQuery(categories, friendsIds, currentLocation, title, distance, user) {
    return {
        'function_score': {
            query: {
                filtered: {
                    query: {
                        bool: {
                            must: [
                                {
                                    terms: {
                                        categoryId: categories
                                    }
                                }, {
                                    match: {
                                        city: user.city
                                    }
                                }, {
                                    range: {
                                        endDateTime: {
                                            gte: "now"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    filter: {
                        bool: {
                            must: [
                                privacyQuery(user, friendsIds)
                            ]
                        }
                    },
                }
            },
            functions: [
                {
                    exp: {
                        startDateTime: {
                            origin: "now",
                            offset: '0',
                            decay: 0.3,
                            scale: '10d'
                        }
                    }
                }
            ],
            score_mode: 'multiply'
        }
    };
}

function getNearQuery(categories, friendsIds, currentLocation, title, distance, user) {
    return {
        'function_score': {
            query: {
                filtered: {
                    query: {
                        bool: {
                            must: [
                                {
                                    terms: {
                                        categoryId: categories
                                    }
                                }, {
                                    match: {
                                        city: user.city
                                    }
                                }, {
                                    range: {
                                        endDateTime: {
                                            gte: "now"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    filter: {
                        bool: {
                            must: [
                                {
                                    'geo_distance': {
                                        distance: distance || '25km',
                                        location: currentLocation
                                    }
                                },
                                privacyQuery(user, friendsIds)
                            ]
                        }
                    },
                }
            },
            functions: [
                {
                    gauss: {
                        location: {
                            origin: currentLocation,
                            offset: '100m',
                            decay: 0.3,
                            scale: '25000m'
                        }
                    }
                }
            ],
            score_mode: 'multiply'
        }
    };
}

function getFriendsQuery(categories, friendsIds, currentLocation, title, distance, user) {
    return {
        'function_score': {
            query: {
                filtered: {
                    query: {
                        bool: {
                            must: [
                                {
                                    terms: {
                                        categoryId: categories
                                    }
                                }, {
                                    terms: {
                                        userId: friendsIds
                                    }
                                }, {
                                    match: {
                                        city: user.city
                                    }
                                }, {
                                    range: {
                                        endDateTime: {
                                            gte: "now"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    filter: {
                        bool: {
                            must: [
                                privacyQuery(user, friendsIds)
                            ]
                        }
                    },
                }
            },
            functions: [
                {
                    gauss: {
                        location: {
                            origin: currentLocation,
                            offset: '100m',
                            decay: 0.3,
                            scale: '25000m'
                        }
                    }
                }
            ],
            score_mode: 'multiply'
        }
    };
}

searchEvent.saveToElastic = function (req, cb) {
    client.index({
        index: 'events',
        id: req.body.id,
        type: 'event',
        body: req.body
    }, function (err, res) {
        logger.trace('save event to elastic search', req.body.title);
        cb(err, res);
    });
};
searchEvent.deleteFromElastic = function (req, cb) {
    client.delete({
        index: 'events',
        id: req.body.id,
        type: 'event'
    }, function () {
        logger.trace('delete event from elastic search', req.body.id);
        cb(null, {
            answer: 1
        });
    });
};

var prepareSearchResult = function (req, userId, searchResult, cb) {
    getUserFriends(userId, function (err, userFriendsIds) {
        async.map(searchResult.hits.hits, function (event, cbMap) {
            getEvent(req.user, event._id, function (err, event) {
                cbMap(err, event);
            });
        }, function (err, results) {
            var resultWithStatistics = _.merge({},
                searchResult);
            resultWithStatistics.hits.hits = _.compact(results);
            cb(!!err, resultWithStatistics);
        });
    });
};

/**
 * @api {get} api/search/event/feed/all?lat=__?lon=__?distance=__km Searching all events
 * @apiDescription Search of all events in radius of current point, Returns array
 * @apiName SearchEventByAll
 * @apiGroup Search
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 * @apiParam {Float} lat Latitude to search about
 * @apiParam {Float} lon Longitude to search about
 * @apiParam {Float} distance Distance to search about. Default '15km'.
 * @apiParam {String} title Filter by partial text of title or address
 * @apiPermission User
 *
 * @apiError 400 JSON has information about error
 */

/**
 * @api {get} api/search/event/feed/now?lat=__?lon=__?distance=__km Searching events by time
 * @apiDescription Search of all events in radius of current point, Returns array
 * @apiName SearchEventByTime
 * @apiGroup Search
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 * @apiParam {Float} lat Latitude to search about
 * @apiParam {Float} lon Longitude to search about
 * @apiParam {Float} distance Distance to search about. Default '15km'.
 * @apiParam {String} title Filter by partial text of title or address
 * @apiPermission User
 *
 * @apiError 400 JSON has information about error
 */

/**
 * @api {get} api/search/event/feed/near?lat=__?lon=__?distance=__km Searching events by location
 * @apiDescription Search of all events in radius of current point, Returns array
 * @apiName SearchEventByLocation
 * @apiGroup Search
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 * @apiParam {Float} lat Latitude to search about
 * @apiParam {Float} lon Longitude to search about
 * @apiParam {Float} distance Distance to search about. Default '15km'.
 * @apiParam {String} title Filter by partial text of title or address
 * @apiPermission User
 *
 * @apiError 400 JSON has information about error
 */

/**
 * @api {get} api/search/event/feed/friends?lat=__?lon=__?distance=__km Searching events by friends
 * @apiDescription Search of all events in radius of current point, Returns array
 * @apiName SearchEventByFriends
 * @apiGroup Search
 * @apiParam {Number} offset Skip some events (default: 0)
 * @apiParam {Number} limit Limit amount of events (default: 100)
 * @apiParam {Float} lat Latitude to search about
 * @apiParam {Float} lon Longitude to search about
 * @apiParam {Float} distance Distance to search about. Default '15km'.
 * @apiParam {String} title Filter by partial text of title or address
 * @apiPermission User
 *
 * @apiError 400 JSON has information about error
 */
searchEvent.feed = function (searchType) {
    return function (req, callback) {
        var currentLocation = {
            lon: req.query.lon ? parseFloat(req.query.lon) : 37.798546,
            lat: req.query.lat ? parseFloat(req.query.lat) : 55.798546
        };
        async.waterfall([
                function (cb) {
                    getUserCategories(req.user.id, cb);
                },
                function (categories, cb) {
                    getUser(req.user, req.user.id, function (err, user) {
                        cb(err, user, categories);
                    });
                },
                function (user, categories, cb) {
                    getUserFriends(req.user.id, function (err,
                                                          userFriendsIds) {
                        cb(err, categories, userFriendsIds, user);
                    });
                },
                function (categories, userFriendsIds, user, cb) {
                    var searchParamsByType = {};
                    if (searchType === 'all') {
                        searchParamsByType = {
                            body: {
                                query: getAllQuery(categories,
                                    userFriendsIds,
                                    currentLocation,
                                    req.query.title,
                                    req.query.distance,
                                    user)
                            }
                        };
                    } else if (searchType === 'now') {
                        searchParamsByType = {
                            body: {
                                query: getNowQuery(categories,
                                    userFriendsIds,
                                    currentLocation,
                                    req.query.title,
                                    req.query.distance,
                                    user)
                            }
                        };
                    } else if (searchType === 'near') {
                        searchParamsByType = {
                            body: {
                                query: getNearQuery(categories,
                                    userFriendsIds,
                                    currentLocation,
                                    req.query.title,
                                    req.query.distance,
                                    user)
                            }
                        };
                    } else if (searchType === 'friends') {
                        searchParamsByType = {
                            body: {
                                query: getFriendsQuery(categories,
                                    userFriendsIds,
                                    currentLocation,
                                    req.query.title,
                                    req.query.distance,
                                    user)
                            }
                        };
                    }
                    var searchParamsBase = {
                        index: 'events',
                        size: req.query.limit || 100,
                        from: req.query.offset || 0,
                        body: {
                            fields: [
                                '_source'
                            ],
                            script_fields: {
                                distance: {
                                    params: currentLocation,
                                    script_file: "distance"
                                }
                            }
                        }
                    };
                    if (req.query.title && req.query.title.length) {
                        searchParamsByType.body.query.function_score.query.filtered.query.bool.must.push(
                            {
                                multi_match: {
                                    query: req.query.title,
                                    fields: [
                                        'title',
                                        'address'
                                    ]
                                }
                            });
                    }
                    var searchParams = _.merge(searchParamsBase,
                        searchParamsByType);
                    client.search(searchParams, function (err, results) {
                        if (err) {
                            return cb(err);
                        }
                        cb(!!err, req, req.user.id, results);
                    });
                }
                ,
                prepareSearchResult
            ],
            function (err, results) {
                callback(err, results);
            }
        )
        ;
    };
};

module.exports = searchEvent;
