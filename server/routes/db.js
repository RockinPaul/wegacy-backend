'use strict';

var express = require('express');
var router = express.Router();
var config = require('../config');
var rpc = require('../common/amqp/amqp-rpc');
var request = require('supertest');
var security = require('../common/security');

function createUser(err, req, res, result) {
    if (err || !result) {
        return res.status(400).send(err);
    }
    request('http://' + config.server.address + ':' + config.server.port +
            '/')
        .post('api/oauth/token')
        .type('application/x-www-form-urlencoded')
        .send({
            grant_type: 'password',
            client_id: config.security.clientApp.client_id,
            client_secret: config.security.clientApp.client_secret,
            username: req.body.email,
            password: req.body.password
        })
        .end(function(err, resAuth) {
            result.oauth = resAuth.body;
            res.status(200).send(result);
        });
}

function createUserBySocial(err, req, res, result) {
    if (err || !result) {
        return res.status(400).send(err);
    }
    request('http://' + config.server.address + ':' + config.server.port +
            '/')
        .post('api/oauth/token')
        .type('application/x-www-form-urlencoded')
        .send({
            grant_type: 'urn:custom:social',
            client_id: config.security.clientApp.client_id,
            client_secret: config.security.clientApp.client_secret,
            social_type: req.params.socialType,
            social_id: req.body.id
        })
        .end(function(err, resAuth) {
            result.oauth = resAuth.body;
            res.status(200).send(result);
        });
}

function installRoute(app) {
    var oauth = app.oauth.authorise();
    router.get('/test', rpc.query('get/db/test'));

    router.post('/user/error/:id', oauth, security('user'), rpc.query(
        'post/db/user/error/:id'));
    router.get('/user/error', oauth, security('user'), rpc.query(
        'get/db/user/error'));

    router.get('/user/follow', oauth, security('user'), rpc.query(
        'get/db/user/follow'));
    router.get('/user/following', oauth, security('user'), rpc.query(
        'get/db/user/following'));

    router.post('/user/abuse/:id', oauth, security('user'), rpc.query(
        'post/db/user/abuse/:id'));

    router.get('/user/categories/list', oauth, security('user'), rpc.query(
        'get/db/user/categories/list'));
    router.get('/user/categories', oauth, security('user'), rpc.query(
        'get/db/user/categories'));
    router.put('/user/categories', oauth, security('user'), rpc.query(
        'put/db/user/categories'));

    router.put('/want', oauth, security('user'), rpc.query(
        'put/db/want'));
    router.get('/want', oauth, security('user'), rpc.query(
        'get/db/want'));
    router.get('/want/search', oauth, security('user'), rpc.query(
        'get/db/want/search'));
    router.put('/want/join', oauth, security('user'), rpc.query(
        'put/db/want/join'));
    router.put('/want/refuse', oauth, security('user'), rpc.query(
        'put/db/want/refuse'));

    router.get('/want/match/comment/:id', oauth, security('user'), rpc.query(
        'get/db/want/match/comment/:id'));
    router.post('/want/match/comment/:id', oauth, security('user'), rpc.query(
        'post/db/want/match/comment/:id'));
    router.delete('/want/match/comment/:id', oauth, security('user'), rpc.query(
        'delete/db/want/match/comment/:id'));

    router.get('/user/going', oauth, security('user'), rpc.query('get/db/user/going'));
    router.put('/user/going/:eventId', oauth, security('user'), rpc.query(
        'put/db/user/going/:eventId'));
    router.delete('/user/going/:eventId', oauth, security('user'), rpc.query(
        'delete/db/user/going/:eventId'));

    router.get('/users', oauth, security('user'), rpc.query('get/db/users'));

    router.get('/user/friends', oauth, security('user'), rpc.query('get/db/user/friends'));

    router.get('/user/requests', oauth, security('user'), rpc.query(
        'get/db/user/requests'));

    router.get('/user/invites', oauth, security('user'), rpc.query(
        'get/db/user/invites'));

    router.get('/user/:id', oauth, security('user'), rpc.query('get/db/user/:id'));
    router.get('/user', oauth, security('user'), rpc.query('get/db/user'));
    router.post('/user', rpc.query('post/db/user', createUser));

    router.put('/user/password', oauth, security('user'), rpc.query('put/db/user/password'));

    router.put('/user/:id', oauth, security('user'), rpc.query('put/db/user/:id'));
    router.get('/user/:userId/friends', oauth, security('user'), rpc.query(
        'get/db/user/:userId/friends'));

    router.put('/user', oauth, security('user'), rpc.query('put/db/user'));
    router.get('/users/search', oauth, security('user'), rpc.query('get/db/users/search'));
    router.delete('/user', oauth, security('user'), rpc.query('delete/db/user'));
    router.post('/user/social/:socialType', rpc.query(
        'post/db/user/social/:socialType', createUserBySocial));


    router.post('/user/follow/:id', oauth, security('user'), rpc.query(
        'post/db/user/follow/:id'));
    router.delete('/user/follow/:id', oauth, security('user'), rpc.query(
        'delete/db/user/follow/:id'));

    router.get('/events', oauth, security('user'), rpc.query('get/db/events'));
    router.get('/events/visited', oauth, security('user'), rpc.query('get/db/events/visited'));
    router.get('/event/:id', oauth, security('user'), rpc.query('get/db/event/:id'));
    router.put('/event/:id', oauth, security('user'), rpc.query('put/db/event/:id'));
    router.post('/event', oauth, security('user'), rpc.query('post/db/event'));
    router.get('/events/search', oauth, security('user'), rpc.query('get/db/events/search'));
    router.delete('/event/:eventId', oauth, security('user'), rpc.query('delete/db/event/:eventId'));

    router.get('/event/:eventId/going', oauth, security('user'),
        rpc.query('get/db/event/:eventId/going'));

    router.post('/event/:eventId/invite', oauth, security('user'), rpc.query(
        'post/event/:eventId/invite'));
    router.post('/event/:eventId/invite/:userId', oauth, security('user'), rpc.query(
        'post/event/:eventId/invite/:userId'));
    router.delete('/event/:eventId/invite/:userId', oauth, security('user'), rpc.query(
        'delete/event/:eventId/invite/:userId'));

    router.post('/event/like/:id', oauth, security('user'), rpc.query(
        'post/db/event/like/:id'));
    router.delete('/event/like/:id', oauth, security('user'), rpc.query(
        'delete/db/event/like/:id'));

    router.get('/event/comment/:id', oauth, security('user'), rpc.query(
        'get/db/event/comment/:id'));
    router.post('/event/comment/:id', oauth, security('user'), rpc.query(
        'post/db/event/comment/:id'));
    router.delete('/event/comment/:id', oauth, security('user'), rpc.query(
        'delete/db/event/comment/:id'));

    router.get('/event/photo/:id', oauth, security('user'), rpc.query(
        'get/db/event/photo/:id'));
    router.post('/event/photo/:id', oauth, security('user'), rpc.query(
        'post/db/event/photo/:id'));
    router.delete('/event/photo/:id', oauth, security('user'), rpc.query(
        'delete/db/event/photo/:id'));

    router.get('/activity', oauth, security('user'), rpc.query(
        'get/feed'));

    router.get('/instagram', rpc.query(
        'get/db/instagram'));
    router.post('/instagram', rpc.query(
        'post/db/instagram'));

    router.post('/instagram/subscribe/:eventId', rpc.query(
        'post/db/instagram/subscribe/:eventId'));
    router.post('/instagram/refresh/:eventId', rpc.query(
        'post/db/instagram/refresh/:eventId'));


    router.get('/instagram/subscriptions', rpc.query(
        'get/db/instagram/subscriptions'));
    router.get('/instagram/subscriptions/unsubscribe', rpc.query(
        'get/db/instagram/subscriptions/unsubscribe'));
    return router;
}

module.exports = function(app) {
    return installRoute(app);
};
