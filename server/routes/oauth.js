'use strict';
var express = require('express');
var router = express.Router();
var config = require('../config');
var logger = require('../config/logger');
var oauth = require('oauthio');
var csrf = require('csurf');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var request = require('supertest');

oauth.initialize(config.oauth.appKey, config.oauth.appSecret);

var createUserBySocial = function (result, me, done) {
    logger.info(result);
    request(global.url || config.url).post('api/db/user/social/' + result.provider).expect(200).send({
            id: result.user_id.toString(), info: {
                email: null,
                first_name: me.firstname,
                gender: "male",
                id: result.user_id.toString(),
                last_name: me.lastname,
                link: "",
                locale: "ru_RU",
                name: me.name
            }
        }).end(function (err, res) {
            logger.info('done', res.body, res.err);
            done(res.body.oauth);
        });
};

function installRoute(app) {
    router.use(cookieParser());
    router.use(session({secret: 'keyboard cat'}))

    router.use(csrf());
    router.use(function (req, res, next) {
        res.cookie('XSRF-TOKEN', req.csrfToken());
        next();
    });
    router.get('/redirect', oauth.redirect(function (result, req, res) {
        result.me().done(function (me) {
            var authParams = {
                grant_type: 'urn:custom:social',
                client_id: config.security.clientApp.client_id,
                client_secret: config.security.clientApp.client_secret,
                social_type: result.provider,
                social_id: result.user_id
            };
            logger.info(1, authParams);
            request(global.url || config.url).post('api/oauth/token').type('application/x-www-form-urlencoded').send(authParams).end(function (err, resAuth) {
                    if (resAuth.body && resAuth.body.code === 400) {
                        logger.info('NEED CREATE');
                        logger.log(JSON.stringify(me, null, '  '));
                        logger.log(JSON.stringify(result, null, '  '));
                        createUserBySocial(result, me, function (oauth) {
                            return res.redirect('/webcontrol.html#/signin?access=' + JSON.stringify(oauth));
                        });
                    } else {
                        return res.redirect('/webcontrol.html#/signin?access=' + JSON.stringify(resAuth.body));
                    }
                });
        });
    }));

    router.get('/vk/signin', oauth.auth('vk', '' + config.url + 'api/oauth/redirect'));
    router.get('/facebook/signin', oauth.auth('facebook', '' + config.url + 'api/oauth/redirect'));
    return router;
}

module.exports = function (app) {
    return installRoute(app);
};
