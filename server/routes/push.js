'use strict';

var express = require('express');
var router = express.Router();
var rpc = require('../common/amqp/amqp-rpc');

function installRoute() {
    router.get('/apple/test', rpc.query('get/push/apple/test'));
    return router;
}

module.exports = function() {
    return installRoute();
};

