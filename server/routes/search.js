'use strict';
var express = require('express');
var router = express.Router();
var rpc = require('../common/amqp/amqp-rpc');
var security = require('../common/security');

function installRoute(app) {
    router.get('/reindex', app.oauth.authorise(), security('user'), rpc.query('get/search/reindex'));

    router.get('/event/feed/all', app.oauth.authorise(), security('user'), rpc.query(
        'get/search/event/feed/all'));
    router.get('/event/feed/now', app.oauth.authorise(), security('user'), rpc.query(
        'get/search/event/feed/now'));
    router.get('/event/feed/near', app.oauth.authorise(), security('user'), rpc.query(
        'get/search/event/feed/near'));
    router.get('/event/feed/friends', app.oauth.authorise(), security('user'), rpc.query(
        'get/search/event/feed/friends'));

    return router;
}

module.exports = function(app) {
    return installRoute(app);
};
