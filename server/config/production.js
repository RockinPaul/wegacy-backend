'use strict';
module.exports = {
    root: __dirname+'/../../build',
    url: 'http://wg.pavelzarudnev.ru/',
    server: {
        address: '127.0.0.1'
    },
    amqp: {
        on: true
    }
};
