'use strict';
module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        type: {
            type: DataTypes.ENUM('instagram'),
            defaultValue: 'instagram'
        },
        tag: {
            type: DataTypes.STRING
        },
        verify_token: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1
        },
        confirmed: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.UUIDV1
        }
    };
    var options = {};

    var Subscription = sequelize.define('subscription', attributes, options);
    return Subscription;
};
