'use strict';

module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        matchId: {
            type: DataTypes.UUID,
        },
        userId: {
            type: DataTypes.UUID
        }
    };
    var options = {
    };

    return sequelize.define('matchRefuse', attributes, options);
};
