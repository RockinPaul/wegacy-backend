'use strict';

var config = require('../config');
var logger = require('../config/logger');
/**
 *   @apiDefine UserSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {String} firstName
 *   @apiSuccess {String} middleName
 *   @apiSuccess {String} lastName
 *   @apiSuccess {String} iosAPIKey
 *   @apiSuccess {String} status One of ['notConfirmed' (need email confirm), 'user', 'admin', 'blocked']
 *   @apiSuccess {String} email can be null
 *   @apiSuccess {Array} friends
 *   @apiSuccess {String} job can be null
 *   @apiSuccess {Enum} city default value = 'Moscow'
 *   @apiSuccess {String} avatar can be null. i.e. '4332fsdk-32434k-re43223.png'. Accessed by api/file/:fileId or api/file/:width/:height/:fileId
 *   @apiSuccess {Boolean} isAccountPrivate Show the user in search only for friends. Default value = false
 *   @apiSuccess {Boolean} isNotifyNewEventNear
 *   @apiSuccess {Boolean} isNotifyInvitationToEvent
 *   @apiSuccess {Boolean} isNotifyNewFollower
 *   @apiSuccess {Boolean} isNotifyFriendCreateNewEvent
 *   @apiSuccess {Boolean} isNotifyServiceNotification
 *   @apiSuccess {Boolean} isWantToGo
 *   @apiSuccess {Date} birthdayDate
 *   @apiSuccess {Enum} sex 'male', 'female' or none
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 *
 */

/**
 *   @apiDefine UserParams
 *   @apiParam {String} id UUIDV1
 *   @apiParam {String} firstName
 *   @apiParam {String} middleName
 *   @apiParam {String} lastName
 *   @apiParam {String} iosAPIKey
 *   @apiParam {String} password sha1(salt + password + salt);
 *   @apiParam {String} email can be null
 *   @apiParam {String} status One of ['notConfirmed' (need email confirm), 'user', 'admin', 'blocked']
 *   @apiParam {String} job can be null
 *   @apiParam {Enum} city can be null. Default value = 'Moscow'
 *   @apiParam {String} avatar can be null. i.e. '4332fsdk-32434k-re43223.png'. Accessed by api/file/:fileId or api/file/:width/:height/:fileId
 *   @apiParam {Boolean} isAccountPrivate Show the user in search only for friends. Default value = false
 *   @apiParam {Boolean} isNotifyNewEventNear
 *   @apiParam {Boolean} isNotifyInvitationToEvent
 *   @apiParam {Boolean} isNotifyNewFollower
 *   @apiParam {Boolean} isNotifyFriendCreateNewEvent
 *   @apiParam {Boolean} isNotifyServiceNotification
 *   @apiParam {Boolean} isWantToGo
 *   @apiParam {Date} birthdayDate
 *   @apiParam {Enum} sex 'male', 'female' or none
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */
function queueSendToSearchUserIndex(user, options, fn) {
    user.sendToSearch(function() {
        fn(null, user);
    });
}

function queueRemoveFromSearchUserIndex(user, options, fn) {
    var rpc = require('../common/amqp/amqp-rpc');
    rpc.call('delete/search/user', {
        body: user.toJSON()
    }, function() {
        fn(null, user);
    });
}

module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        firstName: {
            type: DataTypes.STRING,
            unique: false
        },
        middleName: {
            type: DataTypes.STRING,
            unique: false
        },
        lastName: {
            type: DataTypes.STRING,
            unique: false
        },
        iosAPIKey: {
            type: DataTypes.STRING,
            unique: false
        },
        password: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true,
            validate: {
                len: {
                    args: [3, 128],
                    msg: 'Email address must be between 3 and 128 characters length'
                },
                isEmail: {
                    mag: 'Email address must be valid'
                }
            }
        },
        status: {
            type: DataTypes.ENUM('notConfirmed', 'user', 'admin', 'blocked'),
            allowNull: false,
            defaultValue: 'notConfirmed'
        },
        job: {
            type: DataTypes.STRING,
            allowNull: true
        },
        avatar: {
            type: DataTypes.STRING,
            allowNull: true
        },
        city: {
            type: DataTypes.ENUM(config.cities),
            allowNull: true,
            defaultValue: 'Moscow'
        },
        isAccountPrivate: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isNotifyNewEventNear: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        isNotifyInvitationToEvent: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        isNotifyNewFollower: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        isNotifyFriendCreateNewEvent: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        isNotifyServiceNotification: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        birthdayDate: {
            type: DataTypes.DATE,
            allowNull: true
        },
        sex: {
            type: DataTypes.ENUM('male', 'female'),
            allowNull: true
        },
        isWantToGo: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        lat: {
            type: 'DOUBLE'
        },
        lon: {
            type: 'DOUBLE'
        }
    };
    var options = {
        getterMethods: {
            name: function() {
                return [
                    this.getDataValue('firstName'),
                    this.getDataValue('middleName'),
                    this.getDataValue('lastName')
                ].join(' ');
            }
        },
        setterMethods: {
            name: function(fullName) {
                var parts = fullName.split(' ');
                if (parts.length === 1) {
                    this.setDataValue('firstName', parts[0]);
                }
                if (parts.length === 2) {
                    this.setDataValue('lastName', parts[0]);
                    this.setDataValue('firstName', parts[1]);
                }
                if (parts.length === 3) {
                    this.setDataValue('lastName', parts[0]);
                    this.setDataValue('firstName', parts[1]);
                    this.setDataValue('middleName', parts[2]);
                }
            }
        },
        instanceMethods: {
            toJSON: function() {
                var result = this.dataValues;
                delete result.password;
                return result;
            },
            sendToSearch: function(cb) {
                logger.trace('start elastic save user', this.id);
                var rpc = require('../common/amqp/amqp-rpc');
                sequelize.models.user.find({
                    where: {
                        id: this.id
                    },
                    include: [{
                        model: sequelize.models.category
                    }, {
                        model: sequelize.models.user,
                        as: 'userFollow',
                        attributes: ['id']
                    }]
                }).complete(function(err, user) {
                    if(!user) {
                        cb(null);
                    }
                    rpc.call('post/search/user', {
                        body: user.toJSON()
                    }, function(err) {
                        logger.trace(
                            'finish elastic save user');
                        if (err && err.created === false) {
                            return cb(null);
                        }
                        cb(err);
                    });
                });
            }
        },
        hooks: {
            afterDelete: queueRemoveFromSearchUserIndex,
            afterUpdate: queueSendToSearchUserIndex,
            afterCreate: queueSendToSearchUserIndex
        },
        associate: function(models) {
            models.user.hasMany(models.event, {
                onDelete: 'cascade'
            });
            models.user.hasMany(models.oauthAccessToken, {
                onDelete: 'cascade'
            });
            models.user.hasMany(models.oauthRefreshToken, {
                onDelete: 'cascade'
            });
            models.user.hasMany(models.social, {
                onDelete: 'cascade'
            });
            models.user.hasMany(models.file, {
                onDelete: 'cascade'
            });
            models.user.belongsToMany(models.category, {
                through: 'userCategory'
            });
            models.user.belongsToMany(models.event, {
                as: 'going',
                through: models.userGoing
            });
            models.user.belongsToMany(models.user, {
                as: 'userFollow',
                through: models.userFollow
            });
            models.user.hasMany(models.comment);
        }
    };

    var User = sequelize.define('user', attributes, options);
    return User;
};
