'use strict';

function sendToFeed(event, options, fn) {
    var rpc = require('../common/amqp/amqp-rpc');
    rpc.call('put/feed/newInvite', {
        body: event.toJSON()
    }, function() {
        fn(null, event);
    });
}
module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        eventId: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1
        },
        userId: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1
        },
        toUserId: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1
        }
    };
    var options = {
        hooks: {
            afterDelete: [],
            afterCreate: [sendToFeed]
        },
    };

    var Invite = sequelize.define('eventInvite', attributes, options);
    return Invite;
};
