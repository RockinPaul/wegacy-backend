'use strict';
/**
 *   @apiDefine MatchCommentSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {String} text text of comment
 *   @apiSuccess {Guid} replyTo user id of reply
 *   @apiSuccess {Guid} replyToUser user info of reply
 *   @apiSuccess {Guid} mentionUserId user id to mention
 *   @apiSuccess {Guid} mentionUser user info to mention
 *   @apiSuccess {Guid} userId userId author of comment
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 */

/**
 *   @apiDefine MatchCommentParams
 *   @apiParam {String} text
 *   @apiParam {Guid} replyTo user id of reply
 *   @apiParam {Guid} mentionUserId user id to mention
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */

module.exports = function (sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        }, text: {
            type: DataTypes.TEXT, unique: false
        }, replyTo: {
            type: DataTypes.UUID, defaultValue: null
        }, mentionUserId: {
            type: DataTypes.UUID, defaultValue: null
        }
    };
    var options = {
        associate: function (models) {
            models.matchComment.belongsTo(models.user);
            models.matchComment.belongsTo(models.user, {
                as: 'replyToUser',
                foreignKey: 'replyTo'
            });
            models.matchComment.belongsTo(models.user, {
                as: 'mentionUser',
                foreignKey: 'mentionUserId'
            });
        }, hooks: {
        }
    };

    var Comment = sequelize.define('matchComment', attributes, options);
    return Comment;
};
