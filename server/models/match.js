'use strict';
module.exports = function(sequelize, DataTypes) {
    var attributes;
    attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        status: {
            type: DataTypes.ENUM(
                'searching',
                'searchAgain',
                'needJoinAllUsers',
                'allUsersJoined',
                'refused'
            ),
            defaultValue: 'searching'
        },
        eventId: {
            type: DataTypes.UUID
        }
    };
    var options = {
        associate: function(models) {
            models.match.hasMany(models.user);
            models.match.belongsTo(models.event, {
                constraints: false
            });
            models.match.hasMany(models.matchComment, {
                as: 'matchComment',
                foreignKey: 'matchId'
            });
        }
    };
    return sequelize.define('match', attributes, options);
};
