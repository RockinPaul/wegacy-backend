'use strict';
/**
 *   @apiDefine PhotoSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {Guid} urlId urlId of Photo
 *   @apiSuccess {String} hashTag tag of event for Photo
 *   @apiSuccess {Guid} eventId
 *   @apiSuccess {String} externalId id from instagram
 *   @apiSuccess {String} externalUrl url to open photo fullsized
 *   @apiSuccess {String} externalCreatedTime string of time of creation
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 */

/**
 *   @apiDefine PhotoParams
 *   @apiParam {Guid} urlId urlId of Photo
 *   @apiParam {String} hashTag tag of event for Photo
 *   @apiParam {Guid} eventId
 *   @apiParam {String} externalId id from instagram
 *   @apiParam {String} externalUrl url to open photo fullsized
 *   @apiParam {String} externalCreatedTime string of time of creation
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */
module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        urlId: {
            type: DataTypes.STRING
        },
        hashTag: {
            type: DataTypes.STRING
        },
        eventId: {
            type: DataTypes.UUID
        },
        externalId: {
            type: DataTypes.STRING
        },
        externalUrl: {
            type: DataTypes.STRING
        },
        externalCreatedTime: {
            type: DataTypes.STRING
        }
    };
    var options = {};

    var Photo = sequelize.define('photo', attributes, options);
    return Photo;
};
