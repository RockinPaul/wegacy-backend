'use strict';

function sendToFeed(event, options, fn) {
    var rpc = require('../common/amqp/amqp-rpc');
    rpc.call('put/feed/newLike', {
        body: event.toJSON()
    }, function() {
        fn(null, event);
    });
}
module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        }
    };
    var options = {
        hooks: {
            afterDelete: [],
            afterCreate: [sendToFeed]
        },
    };

    var UserLike = sequelize.define('userLike', attributes, options);
    return UserLike;
};
