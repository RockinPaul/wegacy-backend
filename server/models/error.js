'use strict';
/**
 *   @apiDefine ErrorSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {String} text text of abuse
 *   @apiSuccess {String} info text of info about stuff of abuse
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 */

/**
 *   @apiDefine ErrorParam
 *   @apiParam {String} id UUIDV1
 *   @apiParam {String} text text of abuse
 *   @apiParam {String} info text of info about stuff of abuse
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */
module.exports = function(sequelize, DataTypes) {
    var attributes;
    attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        userId: {
            type: DataTypes.UUID,
            canBeNull: true
        },
        text: {
            type: DataTypes.TEXT, unique: false
        },
        info: {
            type: DataTypes.TEXT, unique: false
        }
    };
    return sequelize.define('error', attributes);
};
