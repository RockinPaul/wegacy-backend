'use strict';
module.exports = function(sequelize, DataTypes) {
    var attributes;
    attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        userId: {
            type: DataTypes.UUID
        },
        socialId: {
            type: DataTypes.STRING,
            unique: true
        },
        socialType: {
            type: DataTypes.ENUM('facebook', 'vk')
        },
        info: {
            type: DataTypes.STRING(1024)
        }
    };
    return sequelize.define('social', attributes);
};
