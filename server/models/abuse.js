'use strict';
/**
 *   @apiDefine AbuseSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {String} text text of abuse
 *   @apiSuccess {String} info text of info about stuff of abuse
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 */

/**
 *   @apiDefine AbuseParam
 *   @apiParam {String} id UUIDV1
 *   @apiParam {String} text text of abuse
 *   @apiParam {String} info text of info about stuff of abuse
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */
module.exports = function(sequelize, DataTypes) {
    var attributes;
    attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        userId: {
            type: DataTypes.UUID
        },
        text: {
            type: DataTypes.STRING(1024)
        },
        info: {
            type: DataTypes.STRING(1024)
        }
    };
    return sequelize.define('abuse', attributes);
};
