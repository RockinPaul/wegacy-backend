'use strict';
module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        type: {
            type: DataTypes.ENUM(
                'newEvent',
                'newInvite',
                'newLike',
                'newFollow',
                'newGoing',
                'newComment',
                'other'
            ),
            defaultValue: 'other'
        },
        toUserId: {
            type: DataTypes.UUID
        },
        fromUserId: {
            type: DataTypes.UUID
        },
        sourceId: {
            type: DataTypes.UUID
        },
    };
    var options = {
        indexes: [{
            name: 'sourceId index',
            method: 'BTREE',
            fields: ['sourceId']
        }]
    };

    var Feed = sequelize.define('feed', attributes, options);
    return Feed;
};
