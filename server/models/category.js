'use strict';
module.exports = function (sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING,
            unique: false
        }
    };
    var options = {
        associate: function(models) {
            models.category.belongsToMany(models.user, {through: 'userCategory'});
        }
    };

    var Category = sequelize.define('category', attributes, options);
    return Category;
};
