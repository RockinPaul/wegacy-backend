'use strict';

var async = require('async');
var config = require('../config');
var getTagsFromString = require('../common/tag-from-string');
var logger = require('../config/logger');
/**
 *   @apiDefine EventSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {String} title
 *   @apiSuccess {Object} location {lat:??,lon:??}
 *   @apiSuccess {Enum} city Moscow or Saint Petersburg. Default: 'Moscow'
 *   @apiSuccess {Enum} privacy Privacy of event: 'InviteOnly', 'InvitedGuestsAndFriends', 'OpenInvite', 'Public'
 *   @apiSuccess {String} address address of event.
 *   @apiSuccess {String} hashTag hash for instagram
 *   @apiSuccess {String} categoryId id of category of event. i.e. 2
 *   @apiSuccess {String} cover link to image
 *   @apiSuccess {Text} eventDetails description of event
 *   @apiSuccess {Date} startDateTime date and time of start. i.e. '2015-04-13 12:30:00' (UTC +0)
 *   @apiSuccess {Date} endDateTime date and time of event end. i.e. '2015-04-13 18:00:00' (UTC +0)
 *   @apiSuccess {String} link web address of event. can be null
 *   @apiSuccess {Integer} eventLike likeCounts
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 *
 */

/**
 *   @apiDefine EventParams
 *   @apiParam {String} title
 *   @apiParam {Object} location {lat:??,lon:??}
 *   @apiParam {Enum} city city in address of event. Default: 'Moscow'
 *   @apiParam {Enum} privacy Privacy of event: 'InviteOnly', 'InvitedGuestsAndFriends', 'OpenInvite', 'Public'
 *   @apiParam {String} address address of event.
 *   @apiParam {String} hashTag hash for instagram
 *   @apiParam {String} categoryId id of category of event. i.e. 2
 *   @apiParam {String} cover link to image
 *   @apiParam {Text} eventDetails description of event
 *   @apiParam {Date} startDateTime date and time of start. i.e. '2015-04-13 12:30:00' (UTC +0)
   @apiParam {Date} endDateTime date and time of event end. i.e. '2015-04-13 18:00:00' (UTC +0)
 *   @apiParam {String} link web address of event. can be null
 *   @apiParam {Integer} eventLike likeCounts
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */

function queueRemoveFromSearchEventIndex(event, options, fn) {
    var rpc = require('../common/amqp/amqp-rpc');
    rpc.call('delete/search/event', {
        body: event.toJSON()
    }, function() {
        fn(null, event);
    });
}

function queueSendToSearchEventIndex(event, options, fn) {
    event.sendToSearch(function() {
        fn(null, event);
    });
}

function sendToFeed(event, options, fn) {
    var rpc = require('../common/amqp/amqp-rpc');
    rpc.call('put/feed/newEvent', {
        body: event.toJSON()
    }, function() {
        fn(null, event);
    });
}


function ifTagChanged(event, options, fn) {
    if (event.hashTag !== event._previousDataValues.hashTag &&
        event.hashTag &&
        event.hashTag.length && config.env === 'production') {
        var rpc = require('../common/amqp/amqp-rpc');
        async.each(getTagsFromString(event.hashTag), function(tag, cb) {
            rpc.call('post/db/instagram/subscribe/:eventId', {
                body: {
                    tag: tag
                }
            }, function() {
                cb();
            });
        }, function() {});
    }
    fn(null, event);
}

module.exports = function(sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING,
            unique: false
        },
        cover: {
            type: DataTypes.STRING,
            unique: false
        },
        city: {
            type: DataTypes.ENUM(config.cities),
            unique: false,
            defaultValue: 'Moscow'
        },
        eventDetails: {
            type: DataTypes.TEXT
        },
        address: {
            type: DataTypes.STRING,
            unique: false
        },
        hashTag: {
            type: DataTypes.STRING,
            unique: false
        },
        location: {
            type: DataTypes.STRING,
            get: function() {
                var location = JSON.parse(this.getDataValue('location'));
                var val = {};
                if(location && location.lon) {
                    val.lon = parseFloat(location.lon);
                }
                if(location && location.lng) {
                    val.lon = parseFloat(location.lng);
                }
                if(location && location.lat) {
                    val.lat = parseFloat(location.lat);
                }
                return val;
            },
            set: function(val) {
                if(val && val.lon) {
                    val.lon = parseFloat(val.lon);
                }
                if(val && val.lng) {
                    val.lon = parseFloat(val.lng);
                    val.lng = undefined;
                }
                if(val && val.lat) {
                    val.lat = parseFloat(val.lat);
                }
                return this.setDataValue('location', JSON.stringify(
                    val));
            },
            defaultValue: '{"lon":37.619899,"lat":55.753676}'
        },
        startDateTime: {
            type: DataTypes.DATE
        },
        endDateTime: {
            type: DataTypes.DATE
        },
        privacy: {
            type: DataTypes.ENUM('InviteOnly',
                'InvitedGuestsAndFriends', 'OpenInvite', 'Public'),
            defaultValue: 'Public'
        },
        link: {
            type: DataTypes.STRING
        },
        userId: {
            type: DataTypes.UUID
        },
        likeCount: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        }
    };
    var options = {
        instanceMethods: {
            sendToSearch: function(cb) {
                logger.trace('start elastic save event', this.id);
                sequelize.models.event.find({
                    where: {
                        id: this.id
                    },
                    include: [{
                        model: sequelize.models.user,
                        as: 'userGoing',
                        attributes: ['id', 'avatar']
                    }]
                }).complete(function(err, event) {
                    if (err) {
                        return cb(err);
                    }
                    var rpc = require(
                        '../common/amqp/amqp-rpc');
                    rpc.call('post/search/event', {
                        body: event.toJSON()
                    }, function(err) {
                        logger.trace(
                            'finish elastic save event'
                        );
                        if (err && err.created ===
                            false) {
                            return cb(null);
                        }
                        cb(err);
                    });
                });
            }
        },
        hooks: {
            //beforeUpdate: [saveOldTag],
            afterDelete: [queueRemoveFromSearchEventIndex],
            afterUpdate: [queueSendToSearchEventIndex, ifTagChanged],
            afterCreate: [queueSendToSearchEventIndex, sendToFeed, ifTagChanged]
        },
        associate: function(models) {
            models.event.belongsTo(models.category);
            models.event.belongsToMany(models.user, {
                as: 'userGoing',
                through: models.userGoing
            });
            models.event.belongsToMany(models.user, {
                as: 'userLike',
                through: models.userLike
            });
            models.event.hasMany(models.comment, {
                as: 'eventComment'
            });
            models.event.hasMany(models.photo, {
                as: 'eventPhoto'
            });
        }
    };

    var Event = sequelize.define('event', attributes, options);
    return Event;
};
