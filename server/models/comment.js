'use strict';
/**
 *   @apiDefine CommentSuccess
 *   @apiSuccess {String} id UUIDV1
 *   @apiSuccess {String} text text of comment
 *   @apiSuccess {Guid} replyTo user id of reply
 *   @apiSuccess {Guid} replyToUser user info of reply
 *   @apiSuccess {Guid} mentionUserId user id to mention
 *   @apiSuccess {Guid} mentionUser user info to mention
 *   @apiSuccess {Guid} userId userId author of comment
 *   @apiSuccess {Date} createdAt
 *   @apiSuccess {Date} updatedAt
 */

/**
 *   @apiDefine CommentParams
 *   @apiParam {String} text
 *   @apiParam {Guid} replyTo user id of reply
 *   @apiParam {Guid} mentionUserId user id to mention
 *   @apiParam {Date} createdAt
 *   @apiParam {Date} updatedAt
 */

function sendToFeed(comment, options, fn) {
    var rpc = require('../common/amqp/amqp-rpc');
    rpc.call('put/feed/newComment', {
        body: comment.toJSON()
    }, function () {
        fn(null, comment);
    });
}

module.exports = function (sequelize, DataTypes) {
    var attributes = {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        }, text: {
            type: DataTypes.TEXT, unique: false
        }, replyTo: {
            type: DataTypes.UUID, defaultValue: null
        }, mentionUserId: {
            type: DataTypes.UUID, defaultValue: null
        }
    };
    var options = {
        associate: function (models) {
            models.comment.belongsTo(models.user);
            models.comment.belongsTo(models.user, {
                as: 'replyToUser',
                foreignKey: 'replyTo'
            });
            models.comment.belongsTo(models.user, {
                as: 'mentionUser',
                foreignKey: 'mentionUserId'
            });
        }, hooks: {
            afterCreate: [sendToFeed]
        }
    };

    var Comment = sequelize.define('comment', attributes, options);
    return Comment;
};
