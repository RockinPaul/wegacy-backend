'use strict';

module.exports = function(grunt) {
    grunt.initConfig({
        wait: {
            options: {
                delay: 6000
            }
        },
        env: {
            test: {
                NODE_ENV: 'test'
            }
        },
        shell: {
            initTestDb: {
                command: 'node server/init/init.js'
            }
        },
        watch: {
            scripts: {
                options: {
                    livereload: true,
                    spawn: true
                },
                files: 'server/{,**/}*.js',
                tasks: ['apidoc', 'wait'] //'simplemocha'
            },
            jade: {
                options: {
                    livereload: false,
                    spawn: true
                },
                files: 'app/{,**/}*.jade',
                tasks: ['newer:jade:serve']
            },
            less: {
                options: {
                    livereload: false,
                    spawn: true
                },
                files: 'app/{,**/}*.less',
                tasks: ['less:serve']
            },
            js: {
                options: {
                    livereload: false,
                    spawn: true
                },
                files: ['app/{,**/}*.js', '!app/bower_components/{,**/}*.*'],
                tasks: ['newer:copy:serve']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    'build/{,**/}*.css',
                    'build/{,**/}*.js',
                    '!build/bower_components/{,**/}*.*',
                    'build/{,**/}*.html',
                    '.tmp/{,**/}*.css',
                    '.tmp/{,**/}*.js',
                    '!.tmp/bower_components/{,**/}*.*',
                    '.tmp/{,**/}*.html'
                ]
            }
        },
        apidoc: {
            serve: {
                src: 'server/',
                dest: 'apidoc'
            }
        },
        jade: {
            build: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [{
                    src: "{,**/}*.jade",
                    dest: "build/",
                    cwd: "app/",
                    ext: ".html",
                    expand: true
                }, {
                    src: "webcontrol.jade",
                    dest: ".tmp/",
                    cwd: "app/",
                    ext: ".html",
                    expand: true
                }]
            },
            serve: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [{
                    src: "{,**/}*.jade",
                    dest: ".tmp/",
                    cwd: "app/",
                    ext: ".html",
                    expand: true
                }]
            }
        },

        uglify_old: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'scripts/',
                    src: '*.js',
                    dest: 'scripts/min/',
                    ext: '.min.js'
                }]
            }
        },

        cssmin_old: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css/',
                    src: ['*.css'],
                    dest: 'css/min/',
                    ext: '.min.css'
                }]
            }
        },

        clean: {
            build: [
                "build/{,**/}*"
            ],
            serve: [
                ".tmp/{,**/}*"
            ]
        },
        copy: {
            build: {
                expand: true,
                dot: true,
                cwd: 'app',
                dest: 'build',
                src: [
                    'apidoc/{,**/}*.*',
                    '{,**/}*.{ico,png,txt,js,gif,svg}',
                    '.htaccess'
                ]
            },
            serve: {
                expand: true,
                dot: true,
                cwd: 'app',
                dest: '.tmp',
                src: [
                    'bower_components/{,**/}*.*',
                    '{,**/}*.{ico,png,txt,js,gif,svg}',
                    'apidoc/{,**/}*.*',
                    '.htaccess'
                ]
            }
        },
        useminPrepare: {
            html: ['.tmp/webcontrol.html'],
            options: {
                dest: 'build'
            }
        },
        usemin: {
            html: ['build/{,**/}*.html'],
            css: ['build/{,**/}*.css'],
            js: ['build/{,**/}*.js'],
            options: {
                assetsDirs: [
                    'build'
                ]
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        'build/{,*/}*.js',
                        'build/{,*/}*.css'
                    ]
                }
            }
        },
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat',
                    src: '**/*.js',
                    dest: '.tmp/concat'
                }]
            }
        },
        less: {
            options: {
                paths: [
                    'app'
                ]
            },
            build: {
                files: {
                    'build/app.css': 'app/app.less'
                }
            },
            serve: {
                files: {
                    '.tmp/app.css': 'app/app.less'
                }
            }
        },
        simplemocha: {
            test: {
                options: {
                    reporter: 'list',
                    quiet: false,
                    clearRequireCache: true
                },
                src: 'server/tests/{,**/}*.js'
            }
        },
        mochaTest: {
            test: {
                options: {
                    reporter: 'list',
                    quiet: false,
                    clearRequireCache: true
                },
                src: 'server/tests/{,**/}*.js'
            }
        }


    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-asset-injector');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-rev');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-apidoc');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-simple-mocha');
    grunt.loadNpmTasks('grunt-wait');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-shell');

    grunt.registerTask('compressJs', ['newer:uglify']);
    grunt.registerTask('compressCss', ['newer:cssmin']);
    grunt.registerTask('default', ['uglify', 'cssmin', 'watch']);
    grunt.registerTask('dev', ['serve']);
    grunt.registerTask('serve', [
        'apidoc',
        'clean:serve',
        'copy:serve',
        'jade:serve',
        'less:serve',
        'watch'
    ]);
    grunt.registerTask('build', [
        'apidoc',
        'jade:build',
        'less:build',
        'copy:build',
        'useminPrepare',
        'concat',
        'ngAnnotate',
        'copy:build',
        'uglify',
        'cssmin',
        'rev',
        'usemin'
    ]);
    grunt.registerTask('test-server-watch', [
        'env:test',
        'apidoc',
        'simplemocha',
        'watch'
    ]);
    grunt.registerTask('test-server', [
        'env:test',
        'apidoc',
        'mochaTest'
    ]);
    grunt.registerTask('production', ['clean', 'build']);
};
